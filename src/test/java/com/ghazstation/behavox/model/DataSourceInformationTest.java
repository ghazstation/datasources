package com.ghazstation.behavox.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataSourceInformationTest {
    private static final String DATA_SOURCE_NAME = "NAME";
    private static final String COLUMN_NAME = "COLUMN";
    private static final String DATA_URL = "//uri/somewhere";

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void CsvDataSourceInformation() throws Exception {
        runCSVTest("csv");
    }

    @Test
    public void CsvCacheDataSourceInformation() throws Exception {
        runCSVTest("csv-cache");
    }

    private void runCSVTest(final String dataType) throws Exception {
        val value = CsvDataSourceInformation.builder()
                .dataType(dataType)
                .dataSourceName(DATA_SOURCE_NAME)
                .dataSourceUrl(DATA_URL)
                .build();

        runTest(value);
    }

    private void runTest(DataSourceInformation value) throws Exception {
        val serializedJson = objectMapper.writeValueAsString(value);

        val deserialized = objectMapper.readValue(serializedJson, DataSourceInformation.class);

        assertEquals(value, deserialized);
    }

    @Test
    public void JoinDataSourceInformation() throws Exception {
        runJoinTest("join");
    }

    @Test
    public void JoinCacheDataSourceInformation() throws Exception {
        runJoinTest("join-cache");
    }

    private void runJoinTest(final String dataType) throws Exception {
        val value = JoinDataSourceInformation.builder()
                .dataType(dataType)
                .dataSourceName(DATA_SOURCE_NAME)
                .dataSourceColumn1(COLUMN_NAME + "1")
                .dataSourceColumn2(COLUMN_NAME + "2")
                .dataSourceName1(DATA_SOURCE_NAME + "1")
                .dataSourceName2(DATA_SOURCE_NAME + "2")
                .joinType(JoinType.INNER)
                .build();

        runTest(value);
    }

}