package com.ghazstation.behavox.provider;

import com.ghazstation.behavox.model.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Interface for a specific Data Provider
 * This wraps a specific data source and allow us to interact with it
 */
public interface DataProvider {

    /**
     * gets DataSourceInformation used to create this DataProvider
     * @return DataSourceInformation
     */
    DataSourceInformation getDataSourceInformation();

    /**
     * Gets ColumnHeader in the order they are presented in Table
     * @return
     */
    List<ColumnHeader> getAllDataHeader();

    /**
     * get total available pages in our data source
     * @param pageSize we are calculating total pages based off
     * @return
     */
    int getTotalPages(final int pageSize);

    /**
     * Scans table from begining starting at a specific page
     * @param pageSize page size we are at
     * @param pageNumber (1-MAX_INT) page of data we are on
     * @return ScanPageableTable
     */
    ScanPageableTable scanTable(final int pageSize,
                                final int pageNumber);

    /**
     * Default implementation of queryByColumn
     * Queries are implemented in the Application (and are not utilization functionality of the data source
     * Implementations should override if they can do better than O(N) query
     * @param columnName to search on
     * @param value column value to match
     * @param seekLimit items to seek in query
     * @param seekOffset seek offset as a page
     * @param <T> Type of columnValue
     * @return SeekOffsetPageableTable
     */
    default <T> SeekOffsetPageableTable queryByColumn(final String columnName,
                                                      final T value,
                                                      final int seekLimit,
                                                      final int seekOffset) {
        List<ColumnHeader> headerList = getAllDataHeader();

        int headerN = headerList.size();
        int i;
        for (i= 0; i < headerN; i++) {
            if (headerList.get(i).getColumnName().equals(columnName)) {
                final int pos = i;

                List<TableRow> matchRows = scanTable(seekLimit, seekOffset)
                        .getDataTable().getDataRows()
                        .stream()
                        .filter(row -> row.getValues().get(pos).equals(value))
                        .collect(Collectors.toList());

                return new SeekOffsetPageableTable(matchRows.size() ,seekLimit, getTotalPages(seekLimit), seekOffset,
                        new Table(getAllDataHeader(), matchRows));
            }
        }
        return null;
    }
}
