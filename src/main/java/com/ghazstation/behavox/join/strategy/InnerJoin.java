package com.ghazstation.behavox.join.strategy;

import com.ghazstation.behavox.model.JoinType;
import com.ghazstation.behavox.model.ScanPageableTable;
import com.ghazstation.behavox.model.Table;
import com.ghazstation.behavox.model.TableRow;
import com.ghazstation.behavox.provider.DataProvider;
import lombok.NonNull;

import java.util.List;

public class InnerJoin implements JoinOperation {
    public static final InnerJoin SINGELTON = new InnerJoin();
    @NonNull
    private final JoinHelper joinHelper = JoinHelper.SINGELTON;

    private InnerJoin() {
    }

    public JoinType getJoinType() {
        return JoinType.INNER;
    }

    @Override
    public ScanPageableTable performJoin(@NonNull final DataProvider dataProvider1,
                                         @NonNull final DataProvider dataProvider2,
                                         @NonNull final String column1,
                                         @NonNull final String column2,
                                         final int limit,
                                         final int pageOffset) {
        ScanPageableTable pageableDataTable = dataProvider1.scanTable(limit, pageOffset);
        int totalPages = pageableDataTable.getTotalPages();

        List<TableRow> dataRows = findMatching(pageableDataTable.getDataTable(),
                dataProvider2,
                column1,
                column2,
                limit);

        return new ScanPageableTable(dataRows.size(), totalPages, pageOffset,
                new Table(joinHelper.joinProviderHeaders(dataProvider1, dataProvider2), dataRows));
    }

    private List<TableRow> findMatching(@NonNull final Table dataTable1,
                                        @NonNull final DataProvider dataProvider2,
                                        @NonNull final String column1,
                                        @NonNull final String column2,
                                        final int limit) {
        return joinHelper.findMatching(dataTable1, dataProvider2, column1, column2, limit, null);
    }
}
