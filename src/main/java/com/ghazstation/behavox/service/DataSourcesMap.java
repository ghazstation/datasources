package com.ghazstation.behavox.service;

import com.ghazstation.behavox.model.DataSourceInformation;
import com.ghazstation.behavox.provider.DataProvider;
import lombok.NonNull;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class DataSourcesMap {

    @NonNull
    private final Map<String, DataProvider> map = new ConcurrentHashMap<>();

    public void add(@NonNull final String providerName, @NonNull final DataProvider dataProvider) {
        if (map.containsKey(providerName)) {
            throw new RuntimeException("Can't overrider name");
        }
        map.put(providerName, dataProvider);
    }

    public DataProvider get(@NonNull final String dataProviderName) {
        return map.get(dataProviderName);
    }

    public Map<String, DataSourceInformation> getAllNamInformation() {
        return map.entrySet().stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> entry.getValue().getDataSourceInformation()));
    }
}
