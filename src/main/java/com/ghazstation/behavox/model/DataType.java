package com.ghazstation.behavox.model;

import lombok.NonNull;

public enum DataType {
    STRING {
        @Override
        public Object convert(@NonNull final String val) {
            return val;
        }
    },
    INT {
        @Override
        public Object convert(@NonNull final String val) {
            return Integer.parseInt(val.trim());
        }
    };
   /* More types go here
   TODO: FLOAT, DOUBLE, JSON, BLOB
    */

    abstract public Object convert(String val);

    static public DataType getTypeFromName(@NonNull final String name) {
        return DataType.valueOf(name.toUpperCase());
    }
}