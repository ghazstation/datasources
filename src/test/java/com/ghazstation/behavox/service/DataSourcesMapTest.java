package com.ghazstation.behavox.service;

import com.ghazstation.behavox.model.DataSourceInformation;
import com.ghazstation.behavox.provider.DataProvider;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DataSourcesMapTest {
    private static final String DATA_1 = "dataprovider1";
    private static final String DATA_2 = "dataprovider2";
    private static final String DATA_3 = "dataprovider3";

    @Mock
    private DataProvider dataProvider1;
    @Mock
    private DataProvider dataProvider2;
    @Mock
    private DataProvider dataProvider3;
    @Mock
    private DataSourceInformation dataSourceInformation1;
    @Mock
    private DataSourceInformation dataSourceInformation2;
    @Mock
    private DataSourceInformation dataSourceInformation3;

    DataSourcesMap dataSourcesMap = new DataSourcesMap();

    @Test
    public void getEmpty() {
        assertNull(dataSourcesMap.get(DATA_1));
    }
    @Test
    public void addGet() {
        dataSourcesMap.add(DATA_1, dataProvider1);
        dataSourcesMap.add(DATA_2, dataProvider2);
        assertEquals(dataProvider1, dataSourcesMap.get(DATA_1));
        assertEquals(dataProvider2, dataSourcesMap.get(DATA_2));
    }

    @Test(expected = RuntimeException.class)
    public void overwriteException() {
        dataSourcesMap.add(DATA_1, dataProvider1);
        dataSourcesMap.add(DATA_1, dataProvider2);
    }

    @Test
    public void getAllNamInformationEmpty() {
        val map = dataSourcesMap.getAllNamInformation();
        assertEquals(0, map.size());
    }

    @Test
    public void getAllNamInformation() {
        dataSourcesMap.add(DATA_1, dataProvider1);
        dataSourcesMap.add(DATA_3, dataProvider3);
        dataSourcesMap.add(DATA_2, dataProvider2);

        when(dataProvider1.getDataSourceInformation())
                .thenReturn(dataSourceInformation1);
        when(dataProvider3.getDataSourceInformation())
                .thenReturn(dataSourceInformation3);
        when(dataProvider2.getDataSourceInformation())
                .thenReturn(dataSourceInformation2);

        val map = dataSourcesMap.getAllNamInformation();

        assertEquals(3, map.size());
        assertEquals(dataSourceInformation1, map.get(DATA_1));
        assertEquals(dataSourceInformation3, map.get(DATA_3));
        assertEquals(dataSourceInformation2, map.get(DATA_2));
    }
}