package com.ghazstation.behavox.storage;

import com.ghazstation.behavox.model.CsvDataSourceInformation;
import com.ghazstation.behavox.model.DataSourceInformation;
import com.ghazstation.behavox.helper.TableHelper;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.provider.DataProviderFactory;
import com.ghazstation.behavox.model.ColumnHeader;
import com.ghazstation.behavox.model.DataType;
import com.ghazstation.behavox.model.ScanPageableTable;
import com.ghazstation.behavox.spring.ProdSettings;
import com.ghazstation.behavox.storage.model.ScanPageableKey;
import lombok.val;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CacheDataProviderTest {
    private static final int DEFAULT_PAGE_SIZE = ProdSettings.PAGE_SIZE;

    private static final String DATA_1_SRC = "cache/CSV_1.csv";

    private static final String DATA_SRC_NAME = "Table1";

    private static final DataSourceInformation DATA_SOURCE_INFORMATION =
            CsvDataSourceInformation.builder()
                    .dataSourceUrl(ClassLoader.getSystemResource(DATA_1_SRC).getPath())
                    .dataSourceName(DATA_SRC_NAME)
                    .dataType("csv")
                    .build();

    private ScanPageableTable scanPageableTable = new ScanPageableTable(1, 1, 1, TableHelper.createTable(1,1 ));
    @Mock
    private InMemoryStorage inMemoryStorageMock;
    @Mock
    private DataProvider dataProviderMock;
    @Mock
    private DataProviderFactory dataProviderFactory;

//    private CsvInMemoryDataProviderFactory csvInMemoryDataProviderFactory =
//            new CsvInMemoryDataProviderFactory(new CsvInMemoryLoader(new CsvDataFactory()));
    private CacheDataProviderFactory cacheDataProviderFactory;

    DataProvider cacheDataProvider;

    @Before
    public void setUp() throws Exception {
        cacheDataProviderFactory =
                new CacheDataProviderFactory(dataProviderFactory, new InMemoryStorageFactory<>(inMemoryStorageMock));
        when(dataProviderFactory.create(DATA_SOURCE_INFORMATION))
                .thenReturn(dataProviderMock);

        cacheDataProvider = cacheDataProviderFactory.create(DATA_SOURCE_INFORMATION);

        assertTrue(cacheDataProvider instanceof CacheDataProvider);

        verify(dataProviderFactory).create(DATA_SOURCE_INFORMATION);
    }

    @After
    public void tearDown() throws Exception {
        verifyNoMoreInteractions(inMemoryStorageMock, dataProviderFactory, dataProviderMock);
    }

    @Test
    public void getTotalPages() {
        when(dataProviderMock.getTotalPages(1))
                .thenReturn(3);
        assertEquals(3, cacheDataProvider.getTotalPages(1));
        verify(dataProviderMock).getTotalPages(1);
    }

    @Test
    public void getDataSourceInformation() {
        when(dataProviderMock.getDataSourceInformation())
                .thenReturn(DATA_SOURCE_INFORMATION);
        assertEquals(DATA_SOURCE_INFORMATION, cacheDataProvider.getDataSourceInformation());
        verify(dataProviderMock).getDataSourceInformation();
    }

    @Test
    public void getAllDataHeader() {
        List<ColumnHeader> headerList =  Arrays.asList(createColumHeader("a", 0),
                createColumHeader("c", 1));

        when(dataProviderMock.getAllDataHeader())
                .thenReturn(headerList);

        assertEquals(headerList, cacheDataProvider.getAllDataHeader());

        verify(dataProviderMock).getAllDataHeader();
    }

    private ColumnHeader createColumHeader(String name, int i) {
        return new ColumnHeader(name, DataType.STRING, i);
    }

    @Test
    public void scanTablePageSizeDoesntMatch() {
        when(dataProviderMock.scanTable(1, 1))
                .thenReturn(scanPageableTable);

        assertEquals(scanPageableTable, cacheDataProvider.scanTable(1, 1));

        verify(dataProviderMock).scanTable(1, 1);
   }

    @Test
    public void scanTablePageCacheMiss() {
        val key = new ScanPageableKey(DATA_SRC_NAME, 1);
        when(dataProviderMock.scanTable(DEFAULT_PAGE_SIZE, 1))
                .thenReturn(scanPageableTable);
        when(inMemoryStorageMock.getIfPresent(key))
                .thenReturn(Optional.empty());
        when(dataProviderMock.getDataSourceInformation())
                .thenReturn(DATA_SOURCE_INFORMATION);

        assertEquals(scanPageableTable, cacheDataProvider.scanTable(DEFAULT_PAGE_SIZE, 1));

        verify(dataProviderMock).scanTable(DEFAULT_PAGE_SIZE, 1);
        verify(inMemoryStorageMock).getIfPresent(key);
        verify(dataProviderMock, times(2)).getDataSourceInformation();
        verify(inMemoryStorageMock).put(key, scanPageableTable.getDataTable());
    }

    @Test
    public void scanTablePagCacheHit() {
        val key = new ScanPageableKey(DATA_SRC_NAME, 1);
        when(inMemoryStorageMock.getIfPresent(key))
                .thenReturn(Optional.of(scanPageableTable.getDataTable()));
        when(dataProviderMock.getDataSourceInformation())
                .thenReturn(DATA_SOURCE_INFORMATION);

        assertEquals(scanPageableTable.getDataTable(), cacheDataProvider.scanTable(DEFAULT_PAGE_SIZE, 1).getDataTable());

        verify(inMemoryStorageMock).getIfPresent(key);
        verify(dataProviderMock, times(1)).getDataSourceInformation();
    }
}