package com.ghazstation.behavox.spring;

import com.ghazstation.behavox.csv.CsvInMemoryDataProviderFactory;
import com.ghazstation.behavox.join.JoinDataProviderFactory;
import com.ghazstation.behavox.provider.DataProviderFactory;
import com.ghazstation.behavox.storage.CacheDataProviderFactory;
import com.ghazstation.behavox.storage.InMemoryStorage;
import com.ghazstation.behavox.storage.InMemoryStorageFactory;
import com.ghazstation.behavox.storage.model.ScanPageableKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CacheConfig {

    @Bean(name = "joinCacheDataProviderFactory")
    @Autowired
    public CacheDataProviderFactory joinCacheDataProviderFactory(
            @Qualifier("joinDataProviderFactory") JoinDataProviderFactory joinDataProviderFactory) {
        return createCacheDataProviderFactory(joinDataProviderFactory);
    }

    @Bean(name = "csvCacheDataProviderFactory")
    @Autowired
    public CacheDataProviderFactory csvCacheDataProviderFactory(
            @Qualifier("csvInMemoryDataProviderFactory") CsvInMemoryDataProviderFactory csvInMemoryDataProviderFactory) {
        return createCacheDataProviderFactory(csvInMemoryDataProviderFactory);
    }

    private CacheDataProviderFactory createCacheDataProviderFactory(
            DataProviderFactory dataProviderFactory) {
        return new CacheDataProviderFactory(dataProviderFactory, inMemoryStorageFactory());
    }

    @Bean
    public InMemoryStorageFactory<ScanPageableKey> inMemoryStorageFactory() {
        return new InMemoryStorageFactory<>(new InMemoryStorage<>());
    }
}
