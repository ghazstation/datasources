package com.ghazstation.behavox.storage;

import com.ghazstation.behavox.model.Table;
import lombok.NonNull;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryStorage<K> implements Storage<K> {
    @NonNull
    private final ConcurrentHashMap<K, Table> map = new ConcurrentHashMap<>();

    @Override
    public Optional<Table> getIfPresent(K key) {
        return Optional.ofNullable(map.get(key));
    }

    @Override
    public void put(K key, Table dataTable) {
        map.put(key, dataTable);
    }
}
