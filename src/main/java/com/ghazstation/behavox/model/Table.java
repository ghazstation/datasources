package com.ghazstation.behavox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

/**
 * Wrapper for Table object, a small representation of the data in a data source
 */
@Value
public class Table {
    private List<ColumnHeader> headerRow;
    private List<TableRow> dataRows;

    @JsonCreator
    public Table(@JsonProperty("headerRow") List<ColumnHeader> headerRow,
                 @JsonProperty("dataRows")  List<TableRow> dataRows) {
        this.headerRow = headerRow;
        this.dataRows = dataRows;
    }
}
