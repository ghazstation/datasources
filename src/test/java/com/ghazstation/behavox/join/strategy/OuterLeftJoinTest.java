package com.ghazstation.behavox.join.strategy;

import com.ghazstation.behavox.model.CsvDataSourceInformation;
import com.ghazstation.behavox.model.JoinType;
import com.ghazstation.behavox.csv.CsvDataFactory;
import com.ghazstation.behavox.csv.CsvInMemoryDataProviderFactory;
import com.ghazstation.behavox.csv.CsvInMemoryLoader;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.model.ColumnHeader;
import com.ghazstation.behavox.model.DataType;
import com.ghazstation.behavox.model.TableRow;
import lombok.val;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OuterLeftJoinTest {
    private static final String DATA_1_SRC = "join/CSV_1.csv";
    private static final String DATA_2_SRC = "join/CSV_2.csv";

    private CsvInMemoryDataProviderFactory csvInMemoryDataProviderFactory =
            new CsvInMemoryDataProviderFactory(new CsvInMemoryLoader(new CsvDataFactory()));
    private DataProvider dataProvider1;
    private DataProvider dataProvider2;

    LeftOuterJoin leftOuterJoin = LeftOuterJoin.SINGELTON;

    @Before
    public void setup() throws Exception {
        dataProvider1 = csvInMemoryDataProviderFactory.create(CsvDataSourceInformation.builder()
                .dataSourceUrl(ClassLoader.getSystemResource(DATA_1_SRC).getPath())
                .dataSourceName("Table1")
                .dataType("csv")
                .build());
        dataProvider2 = csvInMemoryDataProviderFactory.create(CsvDataSourceInformation.builder()
                .dataSourceUrl(ClassLoader.getSystemResource(DATA_2_SRC).getPath())
                .dataSourceName("Table2")
                .dataType("csv")
                .build());
    }

    @Test
    public void getJoinType() {
        assertEquals(JoinType.LEFT_OUTER, leftOuterJoin.getJoinType());
    }

    @Test
    public void performJoinAllResults() {
        val result = leftOuterJoin.performJoin(dataProvider1, dataProvider2,
                "a", "d",
                4, 1);

        assertEquals(3, result.getPageSize());
        assertEquals(1, result.getPageNumber());
        assertEquals(1, result.getTotalPages());
        assertTrue(result.isFirst());
        assertTrue(result.isLast());

        assertEquals(3, result.getDataTable().getDataRows().size() );
        assertEquals(new TableRow(Arrays.asList("1", "50", "1.44", "1", "test", "")),
                result.getDataTable().getDataRows().get(0));
        assertEquals(new TableRow(Arrays.asList("2", "10", "3.14", "2", "no", "John Smith")),
                result.getDataTable().getDataRows().get(1));
        assertEquals(new TableRow(Arrays.asList("3", "15", "5.61", null, null, null)),
                result.getDataTable().getDataRows().get(2));

        int i = 0;
        assertEquals(Arrays.asList(createColumHeader("a", i++),
                createColumHeader("b", i++),
                createColumHeader("c", i++),
                createColumHeader("d", i++),
                createColumHeader("e", i++),
                createColumHeader("f", i++)),
                result.getDataTable().getHeaderRow());
    }

    private ColumnHeader createColumHeader(String name, int i) {
        return new ColumnHeader(name, DataType.STRING, i);
    }

    @Test
    public void joinNoMatchNoColumnTable2() {
        val result = leftOuterJoin.performJoin(dataProvider1, dataProvider2,
                "a", "g",
                4, 1);

        assertEquals(0, result.getPageSize());
        assertEquals(1, result.getPageNumber());
        assertEquals(1, result.getTotalPages());
        assertTrue(result.isFirst());
        assertTrue(result.isLast());

        assertEquals(0, result.getDataTable().getDataRows().size());
    }

    @Test
    public void joinNoMatchNoColumnTable1() {
        val result = leftOuterJoin.performJoin(dataProvider1, dataProvider2,
                "g", "d",
                4, 1);

        assertEquals(0, result.getPageSize());
        assertEquals(1, result.getPageNumber());
        assertEquals(1, result.getTotalPages());
        assertTrue(result.isFirst());
        assertTrue(result.isLast());

        assertEquals(0, result.getDataTable().getDataRows().size());
    }
}