package com.ghazstation.behavox.storage;

import com.ghazstation.behavox.model.DataSourceInformation;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.provider.DataProviderFactory;
import com.ghazstation.behavox.storage.model.ScanPageableKey;
import lombok.AllArgsConstructor;
import lombok.NonNull;


@AllArgsConstructor
public class CacheDataProviderFactory implements DataProviderFactory<DataSourceInformation> {

    @NonNull
    private final DataProviderFactory dataProviderFactory;
    @NonNull
    private final InMemoryStorageFactory<ScanPageableKey> storageFactory;

    @Override
    public String getDataProviderType() {
        return dataProviderFactory.getDataProviderType() + "-cache";
    }

    @Override
    public DataProvider create(@NonNull final DataSourceInformation dataSourceInformation) throws Exception {
        return new CacheDataProvider(storageFactory.getStorage(),
                dataProviderFactory.create(dataSourceInformation));
    }
}
