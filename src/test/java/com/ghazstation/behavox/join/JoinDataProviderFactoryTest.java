package com.ghazstation.behavox.join;

import com.ghazstation.behavox.model.JoinDataSourceInformation;
import com.ghazstation.behavox.model.JoinType;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.service.DataSourcesMap;
import lombok.val;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class JoinDataProviderFactoryTest {
    private static final String DATA_SOURCE_NAME = "NAME";
    private static final String COLUMN_NAME = "COLUMN";
    private static final String DATA_1 = "dataProvider1";
    private static final String DATA_2 = "dataProvider2";

    @Mock
    private DataProvider dataProvider1;
    @Mock
    private DataProvider dataProvider2;

    private DataSourcesMap dataSourcesMap;
    private JoinDataProviderFactory joinDataProviderFactory;

    @Before
    public void setup() throws Exception {
        dataSourcesMap = new DataSourcesMap();
        dataSourcesMap.add(DATA_1, dataProvider1);
        dataSourcesMap.add(DATA_2, dataProvider2);
        joinDataProviderFactory = new JoinDataProviderFactory(dataSourcesMap);
    }

    @After
    public void verifyAfter() {
        verifyNoMoreInteractions(dataProvider1, dataProvider1);
    }

    @Test
    public void getDataProviderType() {
        assertEquals("join", joinDataProviderFactory.getDataProviderType());
    }

    @Test
    public void create() {
        val dataSourceInformation = JoinDataSourceInformation.builder()
                .dataType("join")
                .dataSourceName(DATA_SOURCE_NAME)
                .dataSourceColumn1(COLUMN_NAME + "1")
                .dataSourceColumn2(COLUMN_NAME + "2")
                .dataSourceName1(DATA_1)
                .dataSourceName2(DATA_2)
                .joinType(JoinType.INNER)
                .build();

        DataProvider dataProvider = joinDataProviderFactory.create(dataSourceInformation);

        assertEquals(dataSourceInformation, dataProvider.getDataSourceInformation());
    }
}