package com.ghazstation.behavox.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.annotations.ApiModel;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "dataType", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CsvDataSourceInformation.class, name = "csv"),
        @JsonSubTypes.Type(value = CsvDataSourceInformation.class, name = "csv-cache"),
        @JsonSubTypes.Type(value = JoinDataSourceInformation.class, name = "join"),
        @JsonSubTypes.Type(value = JoinDataSourceInformation.class, name = "join-cache")
})
@ApiModel(discriminator="dataType",
        subTypes={CsvDataSourceInformation.class, JoinDataSourceInformation.class})
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public abstract class DataSourceInformation {
    @NotNull
    @Size(min = 3, max = 50)
    @JsonProperty("dataType")
    private String dataType;

    @NotNull
    @Size(min = 3, max = 50)
    @JsonProperty("dataSourceName")
    private String dataSourceName;
}
