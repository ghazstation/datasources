package com.ghazstation.behavox.csv;

import com.ghazstation.behavox.model.CsvDataSourceInformation;
import com.ghazstation.behavox.helper.TableHelper;
import com.ghazstation.behavox.model.Table;
import lombok.val;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CsvInMemoryDataProviderFactoryTest {
    private static final String DATA_SOURCE_NAME = "NAME";
    private static final String DATA_URL = "//uri/somewhere";

    private static final int ROWS = 5;
    private static final int COLS = 3;
    private static final int PAGE_SIZE = 2;
    private static final int EXPECTED_PAGES = 3;

    private Table table;
    @Mock
    private CsvInMemoryLoader csvInMemoryLoader;
    @InjectMocks
    private CsvInMemoryDataProviderFactory csvInMemoryDataProviderFactory;

    @Before
    public void setup() {
        table = TableHelper.createTable(COLS, ROWS);
    }

    @After
    public void verifyAfter() {
        verifyNoMoreInteractions(csvInMemoryLoader);
    }

    @Test
    public void getDataProviderType() {
        assertEquals("csv", csvInMemoryDataProviderFactory.getDataProviderType());
    }

    @Test
    public void create() throws Exception {
        val dataSourceInformation = CsvDataSourceInformation.builder()
                .dataType("csv")
                .dataSourceName(DATA_SOURCE_NAME)
                .dataSourceUrl(DATA_URL)
                .build();

        when(csvInMemoryLoader.load(DATA_URL))
                .thenReturn(table);

        val dataprovider = csvInMemoryDataProviderFactory.create(dataSourceInformation);

        assertEquals(dataSourceInformation, dataprovider.getDataSourceInformation());
        assertEquals(table.getHeaderRow(), dataprovider.getAllDataHeader());
        assertEquals(EXPECTED_PAGES, dataprovider.getTotalPages(PAGE_SIZE));

        verify(csvInMemoryLoader).load(DATA_URL);
    }
}