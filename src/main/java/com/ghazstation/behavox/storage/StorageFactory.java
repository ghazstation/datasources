package com.ghazstation.behavox.storage;

public interface StorageFactory<StorageType> {
    StorageType getStorage();
}
