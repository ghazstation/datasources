package com.ghazstation.behavox.csv;

import com.ghazstation.behavox.model.ColumnHeader;
import com.ghazstation.behavox.model.DataType;
import com.ghazstation.behavox.model.Table;
import com.ghazstation.behavox.model.TableRow;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CsvInMemoryLoaderTest {
    private static final String CSV_FILE_NAME = "CSV_TEST_1.csv";
    private static final List<ColumnHeader> EXPECTED_HEADER =
            Arrays.asList(
                    new ColumnHeader("a", DataType.STRING, 0),
                    new ColumnHeader("b", DataType.INT, 1),
                    new ColumnHeader("c", DataType.STRING, 2));

    private static final List<TableRow> EXPECTED_ROW =
            Arrays.asList(
                    new TableRow(Arrays.asList("1", 50, "1.44")),
                    new TableRow(Arrays.asList("2", 10, "3.14")),
                    new TableRow(Arrays.asList("3", 15, "5.61")));

    private CsvInMemoryLoader csvInMemoryLoader;

    @Before
    public void setup() {
        csvInMemoryLoader = new CsvInMemoryLoader(new CsvDataFactory());
    }

    @Test
    public void load() {
        String resourcePath = ClassLoader.getSystemResource(CSV_FILE_NAME).getPath();

        Table table = csvInMemoryLoader.load(resourcePath);

        assertEquals(EXPECTED_HEADER, table.getHeaderRow());
        assertEquals(EXPECTED_ROW, table.getDataRows());
    }

    @Test
    public void loadFileDoestExist() {
        Table table = csvInMemoryLoader.load("pathdoesntexist");

        assertEquals(null, table);
    }
}