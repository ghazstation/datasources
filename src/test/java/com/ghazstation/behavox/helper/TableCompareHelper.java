package com.ghazstation.behavox.helper;

import com.ghazstation.behavox.model.Table;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TableCompareHelper {
    public static void compare(Table original, Table subset, int rowStart, int rowEnd) {
        assertEquals(original.getHeaderRow(), subset.getHeaderRow());

        int n = rowEnd - rowStart + 1; //inclusive

        if (rowStart == -1) {
            assertTrue(subset.getDataRows().isEmpty());
            return;
        }
        for (int i = 0; i < n; i++) {
            assertEquals(original.getDataRows().get(i + rowStart),
                    subset.getDataRows().get(i));
        }
    }
}
