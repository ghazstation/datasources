package com.ghazstation.behavox.join.strategy;

import com.ghazstation.behavox.model.*;
import com.ghazstation.behavox.csv.CsvDataFactory;
import com.ghazstation.behavox.csv.CsvInMemoryDataProviderFactory;
import com.ghazstation.behavox.csv.CsvInMemoryLoader;
import com.ghazstation.behavox.provider.DataProvider;
import lombok.val;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class InnerJoinTest {
    private static final String DATA_1_SRC = "join/CSV_1.csv";
    private static final String DATA_2_SRC = "join/CSV_2.csv";

    private CsvInMemoryDataProviderFactory csvInMemoryDataProviderFactory =
            new CsvInMemoryDataProviderFactory(new CsvInMemoryLoader(new CsvDataFactory()));
    private DataProvider dataProvider1;
    private DataProvider dataProvider2;

    InnerJoin innerJoin = InnerJoin.SINGELTON;

    @Before
    public void setup() throws Exception {
        dataProvider1 = csvInMemoryDataProviderFactory.create(CsvDataSourceInformation.builder()
                .dataSourceUrl(ClassLoader.getSystemResource(DATA_1_SRC).getPath())
                .dataSourceName("Table1")
                .dataType("csv")
                .build());
        dataProvider2 = csvInMemoryDataProviderFactory.create(CsvDataSourceInformation.builder()
                .dataSourceUrl(ClassLoader.getSystemResource(DATA_2_SRC).getPath())
                .dataSourceName("Table2")
                .dataType("csv")
                .build());
    }

    @Test
    public void getJoinType() {
        assertEquals(JoinType.INNER, innerJoin.getJoinType());
    }

    @Test
    public void performJoinAllResults() {
        val result = innerJoin.performJoin(dataProvider1, dataProvider2,
                "a", "d",
                4, 1);

        assertEquals(2, result.getPageSize());
        assertEquals(1, result.getPageNumber());
        assertEquals(1, result.getTotalPages());
        assertTrue(result.isFirst());
        assertTrue(result.isLast());

        assertEquals(2, result.getDataTable().getDataRows().size());
        assertEquals(new TableRow(Arrays.asList("1", "50", "1.44", "1", "test", "")),
                result.getDataTable().getDataRows().get(0));
        assertEquals(new TableRow(Arrays.asList("2", "10", "3.14", "2", "no", "John Smith")),
                result.getDataTable().getDataRows().get(1));

        int i = 0;
        assertEquals(Arrays.asList(createColumHeader("a", i++),
                createColumHeader("b", i++),
                createColumHeader("c", i++),
                createColumHeader("d", i++),
                createColumHeader("e", i++),
                createColumHeader("f", i++)),
                result.getDataTable().getHeaderRow());
    }

    private ColumnHeader createColumHeader(String name, int i) {
        return new ColumnHeader(name, DataType.STRING, i);
    }

    @Test
    public void joinNoMatch() {
        val result = innerJoin.performJoin(dataProvider1, dataProvider2,
                "a", "e",
                4, 1);

        assertEquals(0, result.getPageSize());
        assertEquals(1, result.getPageNumber());
        assertEquals(1, result.getTotalPages());
        assertTrue(result.isFirst());
        assertTrue(result.isLast());

        assertEquals(0, result.getDataTable().getDataRows().size());
    }
}