package com.ghazstation.behavox.join;

import com.ghazstation.behavox.model.ColumnHeader;
import com.ghazstation.behavox.model.DataSourceInformation;
import com.ghazstation.behavox.model.JoinDataSourceInformation;
import com.ghazstation.behavox.join.strategy.JoinOperation;
import com.ghazstation.behavox.model.ScanPageableTable;
import com.ghazstation.behavox.provider.DataProvider;
import lombok.Builder;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 *
 */
@Builder
public class JoinCompositeDataProvider implements DataProvider {
    @NonNull
    private final DataProvider dataProvider1;
    @NonNull
    private final DataProvider dataProvider2;
    @NonNull
    private final JoinOperation joinStrategy;
    @NonNull
    private final String joinColumn1;
    @NonNull
    private final String joinColumn2;
    @NonNull
    private final JoinDataSourceInformation joinDataSourceInformation;

    @Override
    public int getTotalPages(int pageSize) {
        return dataProvider1.getTotalPages(pageSize);
    }

    @Override
    public DataSourceInformation getDataSourceInformation() {
        return joinDataSourceInformation;
    }

    @Override
    public List<ColumnHeader> getAllDataHeader() {
        List<ColumnHeader> joinedDataHear = new ArrayList<>();
        joinedDataHear.addAll(dataProvider1.getAllDataHeader());
        List<ColumnHeader> list2 = dataProvider2.getAllDataHeader().stream()
                .map(header -> new ColumnHeader(header.getColumnName(),
                        header.getColumnType(),
                        header.getIndex() + joinedDataHear.size()))
                .collect(Collectors.toList());
        joinedDataHear.addAll(list2);
        return joinedDataHear;
    }

    @Override
    public ScanPageableTable scanTable(final int pageSize, final int pageNumber) {
        return joinStrategy.performJoin(dataProvider1, dataProvider2,
                joinColumn1, joinColumn2,
                pageSize, pageNumber);
    }
}
