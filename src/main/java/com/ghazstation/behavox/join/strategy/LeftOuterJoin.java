package com.ghazstation.behavox.join.strategy;

import com.ghazstation.behavox.model.JoinType;
import com.ghazstation.behavox.model.ScanPageableTable;
import com.ghazstation.behavox.model.Table;
import com.ghazstation.behavox.model.TableRow;
import com.ghazstation.behavox.provider.DataProvider;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

public class LeftOuterJoin implements JoinOperation {
    public static final LeftOuterJoin SINGELTON = new LeftOuterJoin();
    @NonNull
    private final JoinHelper joinHelper = JoinHelper.SINGELTON;

    private LeftOuterJoin() {
    }

    public JoinType getJoinType() {
        return JoinType.LEFT_OUTER;
    }

    @Override
    public ScanPageableTable performJoin(@NonNull final DataProvider dataProvider1,
                                         @NonNull final DataProvider dataProvider2,
                                         @NonNull final String column1,
                                         @NonNull final String column2,
                                         final int limit,
                                         final int pageOffset) {
        ScanPageableTable pageableDataTable = dataProvider1.scanTable(limit, pageOffset);
        int totalPages = pageableDataTable.getTotalPages();

        List<TableRow> dataRows = findMatching(pageableDataTable.getDataTable(),
                dataProvider2,
                column1,
                column2,
                limit);

        return new ScanPageableTable(dataRows.size(), totalPages, pageOffset,
                new Table(joinHelper.joinProviderHeaders(dataProvider1, dataProvider2), dataRows));
    }

    private List<TableRow> findMatching(@NonNull final Table dataTable1,
                                        @NonNull final DataProvider dataProvider2,
                                        @NonNull final String column1,
                                        @NonNull final String column2,
                                        final int limit) {
        List<Object> nullList = new ArrayList<>();
        for (int i=0; i < dataProvider2.getAllDataHeader().size(); i++) {
            nullList.add(null);
        }

        return joinHelper.findMatching(dataTable1, dataProvider2, column1, column2, limit, nullList);
    }
}
