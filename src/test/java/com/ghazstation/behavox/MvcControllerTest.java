package com.ghazstation.behavox;

import com.ghazstation.behavox.model.*;
import com.ghazstation.behavox.helper.MvcHelper;
import lombok.val;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext
public class MvcControllerTest {
    private static final String CSV_1_PATH = ClassLoader.getSystemResource("mvc/CSV_1.csv").getPath();
    private static final String CSV_2_PATH = ClassLoader.getSystemResource("mvc/CSV_2.csv").getPath();

    private static final String CSV_1_NAME = "csv_1";
    private static final String CSV_2_NAME = "csv_2";
    private static final String JOIN_NAME_INNER = "join_csv_inner";
    private static final String JOIN_NAME_OUTER = "join_csv_lo";

    private static final String CSV_DATA_TYPE = "csv";
    private static final String JOIN_DATA_TYPE = "join";
    private static final String CACHE_TAG = "-cache";

    private static final List<ColumnHeader> CSV_1_HEADER =
            Arrays.asList(new ColumnHeader("a", DataType.STRING, 0),
                    new ColumnHeader("b", DataType.STRING, 1),
                    new ColumnHeader("c", DataType.STRING, 2)
            );

    private static final List<TableRow> CSV_1_ROWS =
            Arrays.asList(
                    new TableRow(Arrays.asList("1", "50", "1.44")),
                    new TableRow(Arrays.asList("2", "10", "3.14")),
                    new TableRow(Arrays.asList("3", "15", "5.61"))
            );

    private static final List<ColumnHeader> CSV_2_HEADER =
            Arrays.asList(new ColumnHeader("d", DataType.STRING, 0),
                    new ColumnHeader("e", DataType.STRING, 1),
                    new ColumnHeader("f", DataType.STRING, 2));

    private static final List<TableRow> CSV_2_ROWS =
            Arrays.asList(
                    new TableRow(Arrays.asList("1", "test", "")),
                    new TableRow(Arrays.asList("2", "no", "John Smith")),
                    new TableRow(Arrays.asList("4", "ds", "Ralf"))
            );

    @Autowired
    private MockMvc mvc;
    private MvcHelper mvcHelper;

    private String testId;

    @Before
    public void setUp() throws Exception {
        mvcHelper = new MvcHelper(mvc);

        testId = UUID.randomUUID().toString();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addDataProviderCsvGet() throws Exception {
        val input = createCsv1(testId);

        val res = mvcHelper.getDataSource(input.getDataSourceName());

        assertEquals(input, res);
    }

    @Test
    public void addDataProviderCsvCache() throws Exception {
        val input = createCsv1Cache(testId);

        val res = mvcHelper.getDataSource(input.getDataSourceName());

        assertEquals(input, res);
    }

    @Test
    public void addDataProviderJoin() throws Exception {
        createCsv1(testId);
        createCsv2(testId);

        val joinDataSourceInformation = createInnerJoinCsv1And2(testId);

        val res = mvcHelper.getDataSource(joinDataSourceInformation.getDataSourceName());

        assertEquals(joinDataSourceInformation, res);
    }

    @Test
    public void addDataProviderJoinCache() throws Exception {
        createCsv1(testId);
        createCsv2(testId);

        val joinDataSourceInformation = createLeftOuterJoinCsv1And2Cache(testId);

        val res = mvcHelper.getDataSource(joinDataSourceInformation.getDataSourceName());

        assertEquals(joinDataSourceInformation, res);
    }


    @Test
    public void getAllDataProviders() throws Exception {

        Map<String, DataSourceInformation> expectedMap = new HashMap<>();
        val csvDataSourceInformation1 = createCsv1(testId);
        val csvDataSourceInformation2 = createCsv2(testId);
        val csvDataSourceInformation3 = createInnerJoinCsv1And2(testId);

        val allInformation = mvcHelper.getAllDataSources();

        assertThat(allInformation.size(), greaterThanOrEqualTo(3));

        val filterMap = allInformation.stream()
                .filter(info -> info.getDataSourceName().contains(testId))
                .collect(Collectors.toMap(info -> info.getDataSourceName(), info -> info));

        expectedMap.put(csvDataSourceInformation1.getDataSourceName(), csvDataSourceInformation1);
        expectedMap.put(csvDataSourceInformation2.getDataSourceName(), csvDataSourceInformation2);
        expectedMap.put(csvDataSourceInformation3.getDataSourceName(), csvDataSourceInformation3);

        assertEquals(expectedMap.get(csvDataSourceInformation1.getDataSourceName()), filterMap.get(csvDataSourceInformation1.getDataSourceName()));
        assertEquals(expectedMap.get(csvDataSourceInformation2.getDataSourceName()), filterMap.get(csvDataSourceInformation2.getDataSourceName()));
        assertEquals(expectedMap.get(csvDataSourceInformation3.getDataSourceName()), filterMap.get(csvDataSourceInformation3.getDataSourceName()));
    }

    @Test
    public void scanTablePageZero() throws Exception {
        val csvDataSourceInformation = createCsv1(testId);
        String dataSourcename = csvDataSourceInformation.getDataSourceName();

        mvcHelper.scanTabl503s(dataSourcename, 1, 0);
    }

    @Test
    public void scanTablePageOne() throws Exception {
        runCSV1ScanSuccess(1, 1, 3, Arrays.asList(CSV_1_ROWS.get(0)), true, false);
    }

    @Test
    public void scanTablePageTwo() throws Exception {
        runCSV1ScanSuccess(1, 2, 3, Arrays.asList(CSV_1_ROWS.get(1)), false, false);
    }

    @Test
    public void scanTablePageThree() throws Exception {
        runCSV1ScanSuccess(1, 3, 3, Arrays.asList(CSV_1_ROWS.get(2)), false, true);
    }

    @Test
    public void scanTablePageFourEmpty() throws Exception {
        runCSV1ScanFail(1, 4, 3);
    }

    @Test
    public void scanTablePageSizeTwoFirst() throws Exception {
        runCSV1ScanSuccess(2, 1, 2, Arrays.asList(CSV_1_ROWS.get(0), CSV_1_ROWS.get(1)), true, false);
    }

    @Test
    public void scanTablePageSizeTwoLast() throws Exception {
        runCSV1ScanSuccess(2, 2, 2, Arrays.asList(CSV_1_ROWS.get(2)), false, true);
    }

    @Test
    public void scanTablePageSizeAll()throws Exception {
        runCSV1ScanSuccess(3, 1, 1, CSV_1_ROWS, true, true);
    }

    @Test
    public void scanTablePageBigSize() throws Exception {
        runCSV1ScanSuccess(4, 1, 1, CSV_1_ROWS, true, true);
    }

    @Test
    public void scanTablePageVeryBigSize() throws Exception  {
        runCSV1ScanSuccess(10, 1, 1, CSV_1_ROWS, true, true);
    }


    private void runCSV1ScanSuccess(int pageSize, int pageNumber, int totalPages,
                             List<TableRow> tableRows, boolean first, boolean last) throws Exception {
        runCSV1Scan(pageSize, pageNumber, totalPages, pageNumber, tableRows.size(), tableRows, CSV_1_HEADER, first, last);
    }

    private void runCSV1Scan(int pageSize, int pageNumber, int totalPages, int expectedPageNumber, int expectedPageSize,
                             List<TableRow> tableRows, List<ColumnHeader> headers, boolean first, boolean last) throws Exception {
        val csvDataSourceInformation = createCsv1(testId);
        String dataSourcename = csvDataSourceInformation.getDataSourceName();

        val result =
                mvcHelper.scanTable(dataSourcename, pageSize, pageNumber);
        assertEquals(totalPages, result.getTotalPages());
        assertEquals(expectedPageNumber, result.getPageNumber());
        assertEquals(expectedPageSize, result.getPageSize());
        assertEquals(last, result.isLast());
        assertEquals(first, result.isFirst());

        assertEquals(tableRows,
                result.getDataTable().getDataRows());

        assertEquals(headers, result.getDataTable().getHeaderRow());
    }

    private void runCSV1ScanFail(int pageSize, int pageNumber, int totalPages) throws Exception {
        runCSV1Scan(pageSize, pageNumber, totalPages, -1, 0,
                Collections.emptyList(),  Collections.emptyList(), false, false);
    }

    private CsvDataSourceInformation createCsv1Cache(String id) throws Exception {
        return createCSV(CSV_1_NAME +id, CSV_1_PATH, true);
    }

    private CsvDataSourceInformation createCsv2Cache(String id) throws Exception {
        return createCSV(CSV_2_NAME +id, CSV_2_PATH, true);
    }

    private CsvDataSourceInformation createCsv1(String id) throws Exception {
        return createCSV(CSV_1_NAME +id, CSV_1_PATH, false);
    }

    private CsvDataSourceInformation createCsv2(String id) throws Exception {
        return createCSV(CSV_2_NAME +id, CSV_2_PATH, false);
    }

    private CsvDataSourceInformation createCSV(String datasourceName, String datasourceUrl, boolean createCache) throws Exception {
        val input = CsvDataSourceInformation.builder()
                .dataType(CSV_DATA_TYPE + (createCache ? CACHE_TAG : ""))
                .dataSourceName(datasourceName)
                .dataSourceUrl(datasourceUrl)
                .build();

        mvcHelper.runAddDataSource(input);

        return input;
    }


    private JoinDataSourceInformation createInnerJoinCsv1And2Cache(String id) throws Exception {
        return createJoin(JOIN_NAME_INNER + id, CSV_1_NAME + id, CSV_2_NAME + id, JoinType.INNER, true);
    }

    private JoinDataSourceInformation createLeftOuterJoinCsv1And2Cache(String id) throws Exception {
        return createJoin(JOIN_NAME_OUTER + id, CSV_1_NAME + id, CSV_2_NAME + id, JoinType.LEFT_OUTER, true);
    }

    private JoinDataSourceInformation createInnerJoinCsv1And2(String id) throws Exception {
        return createJoin(JOIN_NAME_INNER + id, CSV_1_NAME + id, CSV_2_NAME + id, JoinType.INNER, false);
    }

    private JoinDataSourceInformation createLeftOuterJoinCsv1And2(String id) throws Exception {
        return createJoin(JOIN_NAME_OUTER + id, CSV_1_NAME + id, CSV_2_NAME + id, JoinType.LEFT_OUTER, false);
    }

    private JoinDataSourceInformation createJoin(String datasourceName, String source1, String source2, JoinType joinType, boolean createCache) throws Exception {
        val input = JoinDataSourceInformation.builder()
                .dataType(JOIN_DATA_TYPE + (createCache ? CACHE_TAG : ""))
                .dataSourceName(datasourceName)
                .dataSourceName1(source1)
                .dataSourceName2(source2)
                .dataSourceColumn1("a")
                .dataSourceColumn2("d")
                .joinType(joinType)
                .build();

        mvcHelper.runAddDataSource(input);

        return input;
    }

    @Test
    public void searchByColumnNotFirstPage() throws Exception {
        runQuery("b", "10",
                3, 1, 1,
                Collections.emptyList(), CSV_1_HEADER,
                true, false);
    }


    @Test
    public void searchByColumnSecondPage() throws Exception {
        runQuery("b", "10",
                3, 2, 1,
                Arrays.asList(CSV_1_ROWS.get(1)), CSV_1_HEADER,
                false, false);
    }

    @Test
    public void searchByColumnLastPage() throws Exception {
        runQuery("b", "10",
                3, 3, 1,
                Collections.emptyList(), CSV_1_HEADER,
                false, true);
    }

    private void runQuery(String columnName, String columnValue,
                          int totalPages, int seekPage, int seekLimit,
                          List<TableRow> tableRows, List<ColumnHeader> headers,
                          boolean first, boolean last) throws Exception {
        val csvDataSourceInformation = createCsv1(testId);
        String dataSourcename = csvDataSourceInformation.getDataSourceName();

        val result = mvcHelper.queryByColumn(dataSourcename, columnName, columnValue, seekLimit, seekPage);

        validateQuery(result, totalPages, seekPage, seekLimit, tableRows, headers,
                first, last);
    }

    private void validateQuery(SeekOffsetPageableTable result,
                               int totalPages, int seekPage, int seeLimit,
                               List<TableRow> tableRows, List<ColumnHeader> headers,
                               boolean first, boolean last) throws Exception {
        assertEquals(totalPages, result.getTotalPages());
        assertEquals(seekPage, result.getSeekPage());
        assertEquals(seeLimit, result.getSeekLimit());
        assertEquals(tableRows.size(), result.getFoundRows());
        assertEquals(last, result.isLastOffset());
        assertEquals(first, result.isFirstOffset());

        assertEquals(tableRows,
                result.getDataTable().getDataRows());

        assertEquals(headers, result.getDataTable().getHeaderRow());
    }
}