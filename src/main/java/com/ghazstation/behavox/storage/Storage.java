package com.ghazstation.behavox.storage;

import com.ghazstation.behavox.model.Table;

import java.util.Optional;

public interface Storage<K> {
    Optional<Table> getIfPresent(K key);

    void put(K key, Table dataTable);
}
