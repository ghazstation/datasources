package com.ghazstation.behavox.csv;

import com.ghazstation.behavox.model.*;
import com.ghazstation.behavox.provider.DataProvider;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class InMemoryCsvDataProvider implements DataProvider {
    @NonNull
    private final Table dataTable;
    @NonNull
    private final CsvDataSourceInformation csvDataSourceInformation;

    @Override
    public int getTotalPages(int pageSize) {
        return (int)Math.ceil((double)dataTable.getDataRows().size() / pageSize);
    }

    @Override
    public DataSourceInformation getDataSourceInformation() {
        return csvDataSourceInformation;
    }

    @Override
    public List<ColumnHeader> getAllDataHeader() {
        return dataTable.getHeaderRow();
    }

    @Override
    public ScanPageableTable scanTable(final int pageSize, final int pageNumber) {
        int totalPages = getTotalPages(pageSize);
        List<TableRow> rows = dataTable.getDataRows().stream()
                .skip(pageSize * (pageNumber - 1))
                .limit(pageSize)
                .collect(Collectors.toList());

        if (rows.isEmpty()) {
            return new ScanPageableTable(0, totalPages, -1,
                    new Table(Collections.EMPTY_LIST, Collections.EMPTY_LIST));
        }

        return new ScanPageableTable(rows.size(), totalPages, pageNumber,
                new Table(dataTable.getHeaderRow(), rows));
    }
}
