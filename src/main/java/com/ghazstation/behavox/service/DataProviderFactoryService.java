package com.ghazstation.behavox.service;

import com.ghazstation.behavox.provider.DataProviderFactory;
import com.google.common.collect.Maps;
import lombok.NonNull;

import java.util.List;
import java.util.Map;

public class DataProviderFactoryService {
    @NonNull
    private final Map<String, DataProviderFactory> dataProviderFactoryMap;

    public DataProviderFactoryService(@NonNull final List<DataProviderFactory> dataProviderFactoryList) {
        this.dataProviderFactoryMap =
                Maps.uniqueIndex(dataProviderFactoryList, DataProviderFactory::getDataProviderType);
    }

    public DataProviderFactory getFactoryByType(@NonNull final String type) {
        return dataProviderFactoryMap.get(type);
    }
}
