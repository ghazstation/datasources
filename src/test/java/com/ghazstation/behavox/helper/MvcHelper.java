package com.ghazstation.behavox.helper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.ghazstation.behavox.model.DataSourceInformation;
import com.ghazstation.behavox.model.ScanPageableTable;
import com.ghazstation.behavox.model.SeekOffsetPageableTable;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;


import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AllArgsConstructor
public class MvcHelper {
    private static final String ADD_DATA_SOURCE_API = "/v1/api/addDataSource";
    private static final String GET_ALL_DATA_SOURCE_API = "/v1/api/datasources";
    private static final String GET_DATA_SOURCE_API_FORMAT = "/v1/api/datasources/%s";

    private static final String SCAN_DATA_SOURCE_API = "/v1/api/scan";
    private static final String SEARCH_COLUMN_DATA_SOURCE_API = "/v1/api/searchByColumn";

    private static final ObjectMapper objectMapper = new ObjectMapper();
    static {
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    @NonNull
    private MockMvc mvc;

    public ResultActions runAddDataSource(
            @NonNull final DataSourceInformation dataSourceInformation) throws Exception {
        ResultActions resultActions = mvc.perform(post(ADD_DATA_SOURCE_API)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(dataSourceInformation)))
                .andExpect(status().isCreated());

        return resultActions;
    }

    public List<DataSourceInformation> getAllDataSources() throws Exception {
        ResultActions resultActions = mvc.perform(get(GET_ALL_DATA_SOURCE_API))
                .andExpect(status().isOk());

        return objectMapper.readValue(resultActions.andReturn().getResponse().getContentAsString(),
                new TypeReference<List<DataSourceInformation>>(){});
    }

    public DataSourceInformation getDataSource(String dataSourceName) throws Exception {
        ResultActions resultActions = mvc.perform(get(String.format(GET_DATA_SOURCE_API_FORMAT, dataSourceName)))
                .andExpect(status().isOk());

        return objectMapper.readValue(resultActions.andReturn().getResponse().getContentAsString(),
                DataSourceInformation.class);
    }

    public ScanPageableTable scanTable(String dataSourceName, int pageSize, int pageNumber) throws Exception {
        ResultActions resultActions = mvc.perform(post(SCAN_DATA_SOURCE_API)
                .param("dataProviderName", dataSourceName)
                .param("pageSize", String.valueOf(pageSize))
                .param("pageNumber", String.valueOf(pageNumber)))
                .andExpect(status().isOk());

        return objectMapper.readValue(resultActions.andReturn().getResponse().getContentAsString(),
                ScanPageableTable.class);
    }

    public void scanTabl503s(String dataSourceName, int pageSize, int pageNumber) throws Exception {
        mvc.perform(post(SCAN_DATA_SOURCE_API)
                .param("dataProviderName", dataSourceName)
                .param("pageSize", String.valueOf(pageSize))
                .param("pageNumber", String.valueOf(pageNumber)))
                .andExpect(status().isServiceUnavailable());
    }

    public SeekOffsetPageableTable queryByColumn(String dataSourceName,
                                                 String columnName,
                                                 String columnValue,
                                                 int offsetLimit,
                                                 int offsetPage) throws Exception {
        ResultActions resultActions = mvc.perform(post(SEARCH_COLUMN_DATA_SOURCE_API)
                .param("dataProviderName", dataSourceName)
                .param("columnName", columnName)
                .param("columnValue", columnValue)
                .param("offsetLimit", String.valueOf(offsetLimit))
                .param("offsetPage", String.valueOf(offsetPage)))
                .andExpect(status().isOk());

        return objectMapper.readValue(resultActions.andReturn().getResponse().getContentAsString(),
                SeekOffsetPageableTable.class);
    }

}
