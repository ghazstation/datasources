package com.ghazstation.behavox.provider;

import com.ghazstation.behavox.model.DataSourceInformation;
import lombok.NonNull;

/**
 * A Factory is associated with a specific Data type and accepts specific DataSourceInformation as input
 * @param <T>
 */
public interface DataProviderFactory<T extends DataSourceInformation> {
    /**
     * DataSourceInformation DataType that this factory is assocaited with
     * Must be unique in the sysetm
     * @return
     */
    String getDataProviderType();

    /**
     * Factory will be called with create when a DataSourceInformation with specificed
     * DataProviderType is given to the system
     * @param dataSourceInformation
     * @return new DataProvider
     * @throws Exception
     */
    DataProvider create(@NonNull final T dataSourceInformation) throws Exception;
}
