package com.ghazstation.behavox.storage.model;

import lombok.Value;

@Value
public class ScanPageableKey {
    private String dataSourceName;
    private int pageNumber;
}