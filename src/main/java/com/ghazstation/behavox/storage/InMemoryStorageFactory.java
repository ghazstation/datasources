package com.ghazstation.behavox.storage;

import lombok.AllArgsConstructor;
import lombok.NonNull;

@AllArgsConstructor
public class InMemoryStorageFactory<K> implements StorageFactory<InMemoryStorage> {
    @NonNull
    private final InMemoryStorage<K> inMemoryStorage;

    @Override
    public InMemoryStorage<K> getStorage() {
        return inMemoryStorage;
    }
}
