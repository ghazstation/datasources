package com.ghazstation.behavox.lombok;

import com.ghazstation.behavox.model.*;
import com.ghazstation.behavox.storage.model.ScanPageableKey;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Test;

public class LombokVerifierTest {

    @Test
    public void providerModel() {
        EqualsVerifier.forClass(ColumnHeader.class).verify();
        EqualsVerifier.forClass(ScanPageableTable.class).verify();
        EqualsVerifier.forClass(SeekOffsetPageableTable.class).verify();
        EqualsVerifier.forClass(Table.class).verify();
        EqualsVerifier.forClass(TableRow.class).verify();
    }

    @Test
    public void controllerModel() {
        EqualsVerifier.forClass(ErrorDetails.class).verify();
    }

    @Test
    public void storageModel() {
        EqualsVerifier.forClass(ScanPageableKey.class).verify();
    }
}
