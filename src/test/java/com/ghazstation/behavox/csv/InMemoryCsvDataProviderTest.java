package com.ghazstation.behavox.csv;

import com.ghazstation.behavox.model.CsvDataSourceInformation;
import com.ghazstation.behavox.helper.TableCompareHelper;
import com.ghazstation.behavox.helper.TableHelper;
import com.ghazstation.behavox.model.ScanPageableTable;
import com.ghazstation.behavox.model.Table;
import lombok.val;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.Assert.*;

public class InMemoryCsvDataProviderTest {
    private static final String DATA_SOURCE_NAME = "NAME";
    private static final String DATA_URL = "//uri/somewhere";

    private static final int ROWS = 5;
    private static final int COLS = 3;
    private static final int PAGE_SIZE = 2;
    private static final int EXPECTED_PAGES = 3;

    private static final CsvDataSourceInformation DATA_SOURCE_INFORMATION =
            CsvDataSourceInformation.builder()
                    .dataType("csv")
                    .dataSourceName(DATA_SOURCE_NAME)
                    .dataSourceUrl(DATA_URL)
                    .build();

    private Table table;
    private InMemoryCsvDataProvider dataProvider;

    @Before
    public void setup() {
        table = TableHelper.createTable(COLS, ROWS);

        dataProvider = new InMemoryCsvDataProvider(table, DATA_SOURCE_INFORMATION);
    }

    @Test
    public void getTotalPages() {
        assertEquals(EXPECTED_PAGES, dataProvider.getTotalPages(PAGE_SIZE));
    }

    @Test
    public void getDataSourceInformation() {
        assertEquals(DATA_SOURCE_INFORMATION, dataProvider.getDataSourceInformation());
    }

    @Test
    public void getAllDataHeader() {
        assertEquals(table.getHeaderRow(), dataProvider.getAllDataHeader());
    }

    @Test
    public void scanTableFirstPage() {
        int pageNumber = 1;
        Table scanTable = runScanIsFirst(pageNumber, PAGE_SIZE, EXPECTED_PAGES);

        TableCompareHelper.compare(table, scanTable, 0, 1);
    }

    @Test
    public void scanTablSecondPage() {
        int pageNumber = 2;
        Table scanTable = runScanMiddle(pageNumber, PAGE_SIZE, EXPECTED_PAGES);

        TableCompareHelper.compare(table, scanTable, 2, 3);
    }

    @Test
    public void scanTableLastPage() {
        int pageNumber = 3;
        Table scanTable = runScanIsLast(pageNumber, PAGE_SIZE, EXPECTED_PAGES, 1);

        TableCompareHelper.compare(table, scanTable, 4, 4);
    }

    @Test
    public void scanTableNoMOrePages() {
        int pageNumber = 4;
        Table scanTable = runScanPageOutOrRange(pageNumber, PAGE_SIZE, EXPECTED_PAGES);

        assertEquals(Collections.EMPTY_LIST, scanTable.getDataRows());
        assertEquals(Collections.EMPTY_LIST, scanTable.getHeaderRow());
    }

    @Test
    public void queryByColumnDoesExist() {
        val result = dataProvider.queryByColumn(String.format(TableHelper.COL_NAME, 3),
                String.format(TableHelper.ROW_VALUE, 2, 2),
                PAGE_SIZE, 1);

        assertNull(result);
    }

    @Test
    public void queryByColumnNotInFirst() {
        val result = dataProvider.queryByColumn(String.format(TableHelper.COL_NAME, 2),
                String.format(TableHelper.ROW_VALUE, 2, 2),
                PAGE_SIZE, 1);

        assertTrue(result.isFirstOffset());
        assertFalse(result.isLastOffset());

        assertEquals(0, result.getDataTable().getDataRows().size());
    }

    @Test
    public void queryByColumnSecondPage() {
        val result = dataProvider.queryByColumn(String.format(TableHelper.COL_NAME, 2),
                String.format(TableHelper.ROW_VALUE, 2, 2),
                PAGE_SIZE, 2);

        assertFalse(result.isFirstOffset());
        assertFalse(result.isLastOffset());

        assertEquals(1, result.getDataTable().getDataRows().size());
        assertEquals(table.getDataRows().get(2), result.getDataTable().getDataRows().get(0));
    }

    @Test
    public void queryByColumnLastPageNotFound() {
        val result = dataProvider.queryByColumn(String.format(TableHelper.COL_NAME, 2),
                String.format(TableHelper.ROW_VALUE, 2, 2),
                PAGE_SIZE, 3);

        assertFalse(result.isFirstOffset());
        assertTrue(result.isLastOffset());

        assertEquals(0, result.getDataTable().getDataRows().size());
    }


    private Table runScanIsFirst(int pageNumber, int pageSize, int expectedPages) {
        val pageableResult = runScan(pageNumber, pageSize, expectedPages, pageSize);

        assertFalse(pageableResult.isLast());
        assertTrue(pageableResult.isFirst());

        return pageableResult.getDataTable();
    }

    private Table runScanIsLast(int pageNumber, int pageSize, int expectedPages, int expectedresult) {
        val pageableResult = runScan(pageNumber, pageSize, expectedPages, expectedresult);

        assertTrue(pageableResult.isLast());
        assertFalse(pageableResult.isFirst());

        return pageableResult.getDataTable();
    }

    private Table runScanMiddle(int pageNumber, int pageSize, int expectedPages) {
        val pageableResult = runScan(pageNumber, pageSize, expectedPages, pageSize);

        assertFalse(pageableResult.isLast());
        assertFalse(pageableResult.isFirst());

        return pageableResult.getDataTable();
    }

    private Table runScanPageOutOrRange(int pageNumber, int pageSize, int expectedPages) {
        LocalDateTime timeNow = LocalDateTime.now();
        val pageableResult = dataProvider.scanTable(pageSize, pageNumber);

        assertEquals(expectedPages, pageableResult.getTotalPages());
        assertEquals(-1, pageableResult.getPageNumber());
        assertEquals(0, pageableResult.getPageSize());


        LocalDateTime timeAfter = LocalDateTime.now();

        assertFalse(pageableResult.getCreateTime().isBefore(timeNow));
        assertFalse(pageableResult.getCreateTime().isAfter(timeAfter));

        assertFalse(pageableResult.isLast());
        assertFalse(pageableResult.isFirst());

        return pageableResult.getDataTable();
    }

    private ScanPageableTable runScan(int pageNumber, int pageSize, int expectedPages, int numResults) {
        LocalDateTime timeNow = LocalDateTime.now();
        System.nanoTime();
        val pageableResult = dataProvider.scanTable(pageSize, pageNumber);

        assertEquals(expectedPages, pageableResult.getTotalPages());
        assertEquals(pageNumber, pageableResult.getPageNumber());
        assertEquals(numResults, pageableResult.getPageSize());


        LocalDateTime timeAfter = LocalDateTime.now();

        assertFalse(pageableResult.getCreateTime().isBefore(timeNow));
        assertFalse(pageableResult.getCreateTime().isAfter(timeAfter));

        return pageableResult;
    }
}