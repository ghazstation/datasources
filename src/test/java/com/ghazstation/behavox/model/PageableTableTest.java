package com.ghazstation.behavox.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghazstation.behavox.helper.TableHelper;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PageableTableTest {
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void scanPageableTable() throws Exception {
        val input = new ScanPageableTable(1, 2, 3,
                TableHelper.createTable(4, 5));

        val json = objectMapper.writeValueAsString(input);

        val deserialized =
                objectMapper.readValue(json, ScanPageableTable.class);

        assertEquals(input, deserialized);
    }

    @Test
    public void seekOffsetPageableTable() throws Exception {
        val input = new SeekOffsetPageableTable(1, 2, 3, 4,
                TableHelper.createTable(5, 6));

        val json = objectMapper.writeValueAsString(input);

        val deserialized =
                objectMapper.readValue(json, SeekOffsetPageableTable.class);

        assertEquals(input, deserialized);
    }

}
