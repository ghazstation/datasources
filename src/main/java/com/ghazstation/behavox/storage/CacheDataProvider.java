package com.ghazstation.behavox.storage;

import com.ghazstation.behavox.model.DataSourceInformation;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.model.ScanPageableTable;
import com.ghazstation.behavox.model.Table;
import com.ghazstation.behavox.model.ColumnHeader;
import com.ghazstation.behavox.spring.ProdSettings;
import com.ghazstation.behavox.storage.model.ScanPageableKey;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class CacheDataProvider implements DataProvider {
    private static final int DEFAULT_PAGE_SIZE = ProdSettings.PAGE_SIZE;

    private final InMemoryStorage<ScanPageableKey> pagableKeyInMemoryStorage;
    private final DataProvider dataProvider;

    @Override
    public int getTotalPages(int pageSize) {
        return dataProvider.getTotalPages(pageSize);
    }

    @Override
    public DataSourceInformation getDataSourceInformation() {
        return dataProvider.getDataSourceInformation();
    }

    @Override
    public List<ColumnHeader> getAllDataHeader() {
        return dataProvider.getAllDataHeader();
    }

    @Override
    public ScanPageableTable scanTable(final int pageSize, final int pageNumber) {
        if (pageSize == DEFAULT_PAGE_SIZE) {
            Optional<Table> optionalTable = pagableKeyInMemoryStorage.getIfPresent(
                    new ScanPageableKey(getDataSourceName(), pageNumber));
            if (optionalTable.isPresent()) {
                return new ScanPageableTable(pageSize, pageNumber, pageNumber, optionalTable.get());
            }
        }

        ScanPageableTable pageableTable = dataProvider.scanTable(pageSize, pageNumber);

        if (pageSize == DEFAULT_PAGE_SIZE) {
            pagableKeyInMemoryStorage.put(new ScanPageableKey(getDataSourceName(), pageNumber), pageableTable.getDataTable());
        }

        return pageableTable;
    }

    private String getDataSourceName() {
        return getDataSourceInformation().getDataSourceName();
    }
}
