package com.ghazstation.behavox.storage;

import com.ghazstation.behavox.model.CsvDataSourceInformation;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.provider.DataProviderFactory;
import com.ghazstation.behavox.model.ColumnHeader;
import com.ghazstation.behavox.model.DataType;
import com.ghazstation.behavox.storage.model.ScanPageableKey;
import lombok.val;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CacheDataProviderFactoryTest {
    private static final String DATA_SOURCE_NAME = "NAME";
    private static final String DATA_URL = "//uri/somewhere";

    private static final CsvDataSourceInformation DATA_SOURCE_INFORMATION =
            CsvDataSourceInformation.builder()
                    .dataType("csv")
                    .dataSourceName(DATA_SOURCE_NAME)
                    .dataSourceUrl(DATA_URL)
                    .build();

    @Mock
    private DataProvider dataProvider;
    @Mock
    private DataProviderFactory dataProviderFactory;
    @Mock
    private InMemoryStorageFactory<ScanPageableKey> storageFactory;
    @InjectMocks
    private CacheDataProviderFactory cacheDataProviderFactory;

    @After
    public void verifyAfter() {
        verifyNoMoreInteractions(dataProviderFactory, dataProvider, storageFactory);
    }

    @Test
    public void getDataProviderType() {
        when(dataProviderFactory.getDataProviderType())
                .thenReturn("dataType");
        assertEquals("dataType-cache", cacheDataProviderFactory.getDataProviderType());

        verify(dataProviderFactory).getDataProviderType();
    }

    @Test
    public void create() throws Exception {
        when(dataProviderFactory.create(DATA_SOURCE_INFORMATION))
                .thenReturn(dataProvider);

        val result = cacheDataProviderFactory.create(DATA_SOURCE_INFORMATION);

        when(dataProvider.getDataSourceInformation())
                .thenReturn(DATA_SOURCE_INFORMATION);

        assertEquals(DATA_SOURCE_INFORMATION, result.getDataSourceInformation());

        List<ColumnHeader> header = Arrays.asList(new ColumnHeader("a", DataType.STRING, 0));

        when(dataProvider.getAllDataHeader())
                .thenReturn(header);

        val dataHeader = result.getAllDataHeader();

        assertEquals(header, dataHeader);

        verify(dataProvider).getDataSourceInformation();
        verify(dataProvider).getAllDataHeader();
        verify(dataProviderFactory).create(DATA_SOURCE_INFORMATION);
        verify(storageFactory).getStorage();
    }
}