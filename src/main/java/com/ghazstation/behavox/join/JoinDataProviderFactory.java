package com.ghazstation.behavox.join;

import com.ghazstation.behavox.model.JoinDataSourceInformation;
import com.ghazstation.behavox.model.JoinType;
import com.ghazstation.behavox.join.strategy.JoinOperation;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.provider.DataProviderFactory;
import com.ghazstation.behavox.service.DataSourcesMap;
import com.google.common.collect.Maps;
import lombok.NonNull;

import java.util.Map;

public class JoinDataProviderFactory implements DataProviderFactory<JoinDataSourceInformation> {

    @NonNull
    private final Map<JoinType, JoinOperation> joinStrategyMap;
    @NonNull
    private final DataSourcesMap dataSourcesMap;

    public JoinDataProviderFactory(@NonNull final DataSourcesMap dataSourcesMap) {
        joinStrategyMap =
                Maps.uniqueIndex(JoinOperation.SUPPORETED_JOINS, JoinOperation::getJoinType);
        this.dataSourcesMap = dataSourcesMap;
    }

    @Override
    public String getDataProviderType() {
        return "join";
    }

    @Override
    public DataProvider create(@NonNull final JoinDataSourceInformation dataSourceInformation) {

        JoinOperation joinOperation = joinStrategyMap.get(dataSourceInformation.getJoinType());

        DataProvider dataProvider1 = dataSourcesMap.get(dataSourceInformation.getDataSourceName1());
        DataProvider dataProvider2 = dataSourcesMap.get(dataSourceInformation.getDataSourceName2());
        String column1 = dataSourceInformation.getDataSourceColumn1();
        String column2 = dataSourceInformation.getDataSourceColumn2();

        return JoinCompositeDataProvider.builder()
                .dataProvider1(dataProvider1)
                .dataProvider2(dataProvider2)
                .joinColumn1(column1)
                .joinColumn2(column2)
                .joinStrategy(joinOperation)
                .joinDataSourceInformation(dataSourceInformation)
                .build();
    }
}
