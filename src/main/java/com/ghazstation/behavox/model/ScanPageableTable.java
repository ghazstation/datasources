package com.ghazstation.behavox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.time.LocalDateTime;

/**
 * A Wrapper for a Table with information about the page information for the table
 */
@Value
public class ScanPageableTable {
    private int pageSize;
    private int totalPages;
    private int pageNumber;

    private Table dataTable;

    private LocalDateTime createTime;

    public boolean isLast() {
        return pageNumber == totalPages;
    }

    public boolean isFirst() {
        return pageNumber == 1;
    }

    public ScanPageableTable(int pageSize, int totalPages, int pageNumber, Table dataTable) {
        this(pageSize, totalPages, pageNumber, dataTable, LocalDateTime.now());
    }

    @JsonCreator
    public ScanPageableTable(@JsonProperty("pageSize") int pageSize,
                             @JsonProperty("totalPages") int totalPages,
                             @JsonProperty("pageNumber") int pageNumber,
                             @JsonProperty("dataTable") Table dataTable,
                             @JsonProperty("createTime") LocalDateTime createTime ) {
        this.pageSize = pageSize;
        this.totalPages = totalPages;
        this.pageNumber = pageNumber;
        this.dataTable = dataTable;
        this.createTime = createTime;
    }
}
