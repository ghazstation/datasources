package com.ghazstation.behavox.join;

import com.ghazstation.behavox.model.JoinDataSourceInformation;
import com.ghazstation.behavox.model.JoinType;
import com.ghazstation.behavox.helper.TableHelper;
import com.ghazstation.behavox.join.strategy.JoinOperation;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.model.ColumnHeader;
import com.ghazstation.behavox.model.ScanPageableTable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JoinCompositeDataProviderTest {
    private static final String DATA_SOURCE_NAME = "NAME";
    private static final String COLUMN_NAME = "COLUMN";
    private static final String DATA_1 = "dataProvider1";
    private static final String DATA_2 = "dataProvider2";

    private static final JoinDataSourceInformation DATA_SOURCE_INFORMATION =
            JoinDataSourceInformation.builder()
                    .dataType("join")
                    .dataSourceName(DATA_SOURCE_NAME)
                    .dataSourceColumn1(COLUMN_NAME + "1")
                    .dataSourceColumn2(COLUMN_NAME + "2")
                    .dataSourceName1(DATA_1)
                    .dataSourceName2(DATA_2)
                    .joinType(JoinType.INNER)
                    .build();


    @Mock
    private DataProvider dataProvider1;
    @Mock
    private DataProvider dataProvider2;
    @Mock
    private JoinOperation joinOperation;

    JoinCompositeDataProvider joinCompositeDataProvider;

    @Before
    public void setup() {
        joinCompositeDataProvider = JoinCompositeDataProvider.builder()
                .dataProvider1(dataProvider1)
                .dataProvider2(dataProvider2)
                .joinColumn1(COLUMN_NAME + "A")
                .joinColumn2(COLUMN_NAME + "B")
                .joinStrategy(joinOperation)
                .joinDataSourceInformation(DATA_SOURCE_INFORMATION)
                .build();
    }

    @After
    public void verifyAfter() {
        verifyNoMoreInteractions(dataProvider1, dataProvider1, joinOperation);
    }

    @Test
    public void getTotalPages() {
        when(dataProvider1.getTotalPages(20))
                .thenReturn(50);
        assertEquals(50, joinCompositeDataProvider.getTotalPages(20));

        verify(dataProvider1).getTotalPages(20);
    }

    @Test
    public void getDataSourceInformation() {
        assertEquals(DATA_SOURCE_INFORMATION,
                joinCompositeDataProvider.getDataSourceInformation());
    }

    @Test
    public void getAllDataHeader() {
        List<ColumnHeader> list1 = IntStream.range(0, 3)
                .boxed()
                .map(TableHelper::createColumn)
                .collect(Collectors.toList());

        List<ColumnHeader> list2 = IntStream.range(0, 2)
                .boxed()
                .map(TableHelper::createColumn)
                .collect(Collectors.toList());

        List<ColumnHeader> combinedList = new ArrayList<>();
        combinedList.addAll(list1);
        final int i = 3;
        combinedList.addAll(list2.stream()
                .map(header -> new ColumnHeader(header.getColumnName(),
                        header.getColumnType(),
                        i + header.getIndex()))
                .collect(Collectors.toList()));

        when(dataProvider1.getAllDataHeader())
                .thenReturn(list1);
        when(dataProvider2.getAllDataHeader())
                .thenReturn(list2);

        assertEquals(combinedList, joinCompositeDataProvider.getAllDataHeader());

        verify(dataProvider1).getAllDataHeader();
        verify(dataProvider2).getAllDataHeader();
    }

    @Test
    public void scanTable() {
        ScanPageableTable scanPageableTable =
                new ScanPageableTable(1,4,3, null);
        when(joinOperation.performJoin(dataProvider1, dataProvider2,
                COLUMN_NAME + "A", COLUMN_NAME + "B",
                1, 3))
                .thenReturn(scanPageableTable);

        assertEquals(scanPageableTable, joinCompositeDataProvider.scanTable(1, 3));

        verify(joinOperation).performJoin(dataProvider1, dataProvider2,
                COLUMN_NAME + "A", COLUMN_NAME + "B",
                1, 3);
    }
}