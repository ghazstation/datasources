package com.ghazstation.behavox.service;

import com.ghazstation.behavox.model.DataSourceInformation;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.provider.DataProviderFactory;
import com.ghazstation.behavox.model.ScanPageableTable;
import com.ghazstation.behavox.model.SeekOffsetPageableTable;
import lombok.val;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DataSourcesServiceTest {
    private static final String DATA_PROVIDER_TYPE1 = "DataType1";
    private static final String DATA_PROVIDER_TYPE2 = "DataType2";

    private static final String DATA_1 = "dataprovider1";
    private static final String DATA_2 = "dataprovider2";
    private static final String DATA_3 = "dataprovider3";

    static class TestDataSourceInformation extends DataSourceInformation {
        private boolean isTest = true;

        public TestDataSourceInformation(String dataType,
                                         String dataSourceName,
                                         boolean isTest) {
            super(dataType, dataSourceName);
            this.isTest = isTest;
        }
    }

    @Mock
    DataProviderFactory dataProviderFactory1;
    @Mock
    DataProviderFactory dataProviderFactory2;

    @Mock
    private DataProvider dataProvider1;
    @Mock
    private DataProvider dataProvider2;
    @Mock
    private DataProvider dataProvider3;
    @Mock
    private DataSourceInformation dataSourceInformation1;
    @Mock
    private DataSourceInformation dataSourceInformation2;
    @Mock
    private DataSourceInformation dataSourceInformation3;

    ScanPageableTable scanPageableTableMock =
            new ScanPageableTable(1,2,3, null);
    SeekOffsetPageableTable seekOffsetPageableTableMock =
            new SeekOffsetPageableTable(1,2,3,4, null);

    DataSourcesMap dataSourcesMap;
    DataProviderFactoryService dataProviderFactoryService;
    DataSourcesService dataSourcesService;

    @Before
    public void setUp() {
        when(dataProviderFactory1.getDataProviderType())
                .thenReturn(DATA_PROVIDER_TYPE1);
        when(dataProviderFactory2.getDataProviderType())
                .thenReturn(DATA_PROVIDER_TYPE2);

        dataSourcesMap = new DataSourcesMap();

        dataProviderFactoryService = new DataProviderFactoryService(
                Arrays.asList(dataProviderFactory1, dataProviderFactory2));
        dataSourcesService = new DataSourcesService(dataProviderFactoryService, dataSourcesMap);
    }

    @After
    public void tearDown() {
        verify(dataProviderFactory1).getDataProviderType();
        verify(dataProviderFactory2).getDataProviderType();

        verifyNoMoreInteractions(dataProviderFactory1, dataProviderFactory2);

    }

    @Test
    public void addDataType1() throws Exception {
        TestDataSourceInformation testDataSourceInformation =
                new TestDataSourceInformation(DATA_PROVIDER_TYPE1, DATA_1, true);

        when(dataProviderFactory1.create(testDataSourceInformation))
                .thenReturn(dataProvider1);

        dataSourcesService.add(testDataSourceInformation);

        val provider = dataSourcesService.get(DATA_1);

        assertEquals(dataProvider1, provider);

        verify(dataProviderFactory1).create(testDataSourceInformation);
    }

    @Test
    public void scanTable() {
        dataSourcesMap.add(DATA_1, dataProvider1);

        when(dataProvider1.scanTable(5, 3))
                .thenReturn(scanPageableTableMock);

        val result = dataSourcesService.scanTable(DATA_1, 5, 3);

        assertEquals(scanPageableTableMock, result);
    }

    @Test
    public void queryByColumn() {
        dataSourcesMap.add(DATA_1, dataProvider1);

        when(dataProvider1.queryByColumn("COLUMN", "4", 3, 5))
                .thenReturn(seekOffsetPageableTableMock);

        val result = dataSourcesService.queryByColumn(DATA_1, "COLUMN", "4", 3, 5);

        assertEquals(seekOffsetPageableTableMock, result);
    }

    @Test
    public void getAllNamInformation() {
        dataSourcesMap.add(DATA_1, dataProvider1);
        dataSourcesMap.add(DATA_3, dataProvider3);
        dataSourcesMap.add(DATA_2, dataProvider2);

        when(dataProvider1.getDataSourceInformation())
                .thenReturn(dataSourceInformation1);
        when(dataProvider3.getDataSourceInformation())
                .thenReturn(dataSourceInformation3);
        when(dataProvider2.getDataSourceInformation())
                .thenReturn(dataSourceInformation2);

        val map = dataSourcesService.getAllNamInformation();

        assertEquals(3, map.size());
        assertEquals(dataSourceInformation1, map.get(DATA_1));
        assertEquals(dataSourceInformation3, map.get(DATA_3));
        assertEquals(dataSourceInformation2, map.get(DATA_2));

        verify(dataProvider1).getDataSourceInformation();
        verify(dataProvider2).getDataSourceInformation();
        verify(dataProvider3).getDataSourceInformation();
    }
}