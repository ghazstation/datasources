package com.ghazstation.behavox.controller;

import com.ghazstation.behavox.model.DataSourceInformation;
import com.ghazstation.behavox.model.ScanPageableTable;
import com.ghazstation.behavox.model.SeekOffsetPageableTable;
import com.ghazstation.behavox.service.DataSourcesService;
import com.ghazstation.behavox.spring.ProdSettings;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/api")
@AllArgsConstructor
@Api(description = "API for joining data sources", tags = "Data Sources API")
@Validated
public class DataSourcesController {
    private static final int DEFAULT_PAGE_SIZE = ProdSettings.PAGE_SIZE;

    @Autowired
    private final DataSourcesService dataSourceProviderService;

    @RequestMapping(value = "/addDataSource",
            method = RequestMethod.POST,
            consumes="application/json")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void addDataProvider(@RequestBody @NotNull final DataSourceInformation dataSourceInformation)
            throws Exception {
        dataSourceProviderService.add(dataSourceInformation);
    }

    @RequestMapping(value = "/datasources", method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    public List<DataSourceInformation> getAllDataProviders() {
        return new ArrayList<>(dataSourceProviderService.getAllNamInformation().values());
    }

    @RequestMapping(value = "/datasources/{dataSourceName}", method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    public DataSourceInformation getAllDataProviders(@PathVariable @NotNull String dataSourceName) {
        return dataSourceProviderService.getAllNamInformation().get(dataSourceName);
    }

    @RequestMapping(value = "/scan", method = RequestMethod.POST)
    @ResponseStatus(code = HttpStatus.OK)
    public ScanPageableTable scan(@RequestParam @NotNull final String dataProviderName,
                                  @RequestParam(defaultValue = "100", required = false) @Positive final Integer pageSize,
                                  @RequestParam(defaultValue = "1") @Positive final int pageNumber) {
        return dataSourceProviderService.scanTable(dataProviderName, pageSize!= null ? pageSize : DEFAULT_PAGE_SIZE, pageNumber);
    }

    @RequestMapping(value = "/searchByColumn", method = RequestMethod.POST)
    @ResponseStatus(code = HttpStatus.OK)
    public SeekOffsetPageableTable searchByColumn(@RequestParam @NotNull final String dataProviderName,
                                                  @RequestParam @NotNull final String columnName,
                                                  @RequestParam @NotNull final String columnValue,
                                                  @RequestParam(defaultValue = "100", required = false) @Positive final Integer offsetLimit,
                                                  @RequestParam(defaultValue = "1") @Positive final int offsetPage) {
       return dataSourceProviderService
               .queryByColumn(dataProviderName, columnName, columnValue,
                       offsetLimit != null ? offsetLimit : DEFAULT_PAGE_SIZE, offsetPage);
    }
}
