package com.ghazstation.behavox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

/**
 * Defines the name, DataType and index of a column header in a Table
 */
@Value
public class ColumnHeader {
    private String columnName;
    private DataType columnType;
    private int index;

    @JsonCreator
    public ColumnHeader(@JsonProperty("columnName") String columnName,
                        @JsonProperty("columnType") DataType columnType,
                        @JsonProperty("index") int index) {
        this.columnName = columnName;
        this.columnType = columnType;
        this.index = index;
    }
}