package com.ghazstation.behavox.join.strategy;

import com.ghazstation.behavox.model.ColumnHeader;
import com.ghazstation.behavox.model.SeekOffsetPageableTable;
import com.ghazstation.behavox.model.Table;
import com.ghazstation.behavox.model.TableRow;
import com.ghazstation.behavox.provider.DataProvider;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class JoinHelper {
    protected static final JoinHelper SINGELTON = new JoinHelper();

    public List<ColumnHeader> joinProviderHeaders(@NonNull final DataProvider dataProvider1,
                                                  @NonNull final DataProvider dataProvider2) {
        List<ColumnHeader> joinedDataHear = new ArrayList<>();
        joinedDataHear.addAll(dataProvider1.getAllDataHeader());

        int offset = joinedDataHear.size();

        List<ColumnHeader> offsetDatahear2 = dataProvider2.getAllDataHeader().stream()
                .map(header ->
                        new ColumnHeader(header.getColumnName(),
                                header.getColumnType(),
                                header.getIndex() + offset))
                .collect(Collectors.toList());
        joinedDataHear.addAll(offsetDatahear2);
        return joinedDataHear;
    }

    /* use nullList to do left outer join, if nullList is null, do inner join */
    public List<TableRow> findMatching(@NonNull final Table dataTable1,
                                       @NonNull final DataProvider dataProvider2,
                                       @NonNull final String column1,
                                       @NonNull final String column2,
                                       final int limit,
                                       final List<Object> nullList) {
        final Optional<ColumnHeader> headerOptional = dataTable1.getHeaderRow().stream()
                .filter(row -> row.getColumnName().equals(column1))
                .findFirst();

        if (headerOptional.isPresent()) {
            final List<TableRow> results = new ArrayList<>();
            int pos = headerOptional.get().getIndex();
            for (final TableRow row : dataTable1.getDataRows()) {
                String columnValue = (String)row.getValues().get(pos);

                int dataProvider2TotalPages;
                int pageOffset = 1;

                do {
                    final SeekOffsetPageableTable pageableDataTable =
                            dataProvider2.queryByColumn(column2, columnValue, limit, pageOffset);

                    if (pageableDataTable == null) {
                        return Collections.EMPTY_LIST;
                    }

                    dataProvider2TotalPages = pageableDataTable.getTotalPages();

                    if (pageableDataTable.getDataTable().getDataRows().isEmpty()) {
                        if (nullList != null && !nullList.isEmpty()) {
                            results.add(joinRow(row, new TableRow(nullList)));
                        }
                    } else {
                        final List<TableRow> joinedRows =
                                pageableDataTable.getDataTable().getDataRows()
                                        .stream()
                                        .map(datarow2 -> joinRow(row, datarow2))
                                        .collect(Collectors.toList());
                        results.addAll(joinedRows);
                    }
                    pageOffset++;
                } while (pageOffset <= dataProvider2TotalPages);
            }
            return results;
        }
        return Collections.EMPTY_LIST;
    }

    private TableRow joinRow(final TableRow row1, final TableRow row2) {
        List<Object> newObjects = new ArrayList<>();
        newObjects.addAll(row1.getValues());
        newObjects.addAll(row2.getValues());
        return new TableRow(newObjects);
    }
}
