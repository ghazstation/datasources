package com.ghazstation.behavox.storage;

import com.ghazstation.behavox.helper.TableHelper;
import com.ghazstation.behavox.model.Table;
import com.ghazstation.behavox.storage.model.ScanPageableKey;
import lombok.val;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class InMemoryStorageTest {
    private static final String DATA_SRC_NAME = "Table1";

    InMemoryStorage<ScanPageableKey> inMemoryStorage = new InMemoryStorage<>();

    @Test
    public void cacheMiss() {
        val key = new ScanPageableKey(DATA_SRC_NAME, 1);

        assertEquals(Optional.empty(), inMemoryStorage.getIfPresent(key));
    }

    @Test
    public void putGet() {
        val key = new ScanPageableKey(DATA_SRC_NAME, 1);

        Table table = TableHelper.createTable(1,1 );

        inMemoryStorage.put(key, table);

        assertEquals(table, inMemoryStorage.getIfPresent(key).get());
    }
}