package com.ghazstation.behavox.csv;

import com.ghazstation.behavox.model.CsvDataSourceInformation;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.provider.DataProviderFactory;
import lombok.AllArgsConstructor;
import lombok.NonNull;

@AllArgsConstructor
public class CsvInMemoryDataProviderFactory implements DataProviderFactory<CsvDataSourceInformation> {
    @NonNull
    private final CsvInMemoryLoader csvInMemoryLoader;

    @Override
    public String getDataProviderType() {
        return "csv";
    }

    @Override
    public DataProvider create(@NonNull final CsvDataSourceInformation dataSourceInformation) throws Exception {
        return new InMemoryCsvDataProvider(
                csvInMemoryLoader.load(dataSourceInformation.getDataSourceUrl()), dataSourceInformation);
    }
}
