package com.ghazstation.behavox.spring;

import com.ghazstation.behavox.csv.CsvDataFactory;
import com.ghazstation.behavox.csv.CsvInMemoryDataProviderFactory;
import com.ghazstation.behavox.csv.CsvInMemoryLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CsvConfig {

    @Bean(name = "csvInMemoryDataProviderFactory")
    public CsvInMemoryDataProviderFactory csvInMemoryDataProviderFactory() {
        return new CsvInMemoryDataProviderFactory(csvInMemoryLoader());
    }

    @Bean
    public CsvInMemoryLoader csvInMemoryLoader() {
        return new CsvInMemoryLoader(csvDataFactory());
    }

    @Bean
    public CsvDataFactory csvDataFactory() {
        return new CsvDataFactory();
    }
}

