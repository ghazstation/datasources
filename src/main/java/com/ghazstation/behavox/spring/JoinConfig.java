package com.ghazstation.behavox.spring;

import com.ghazstation.behavox.join.JoinDataProviderFactory;
import com.ghazstation.behavox.service.DataSourcesMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JoinConfig {

    @Autowired
    private DataSourcesMap dataSourcesMap;

    @Bean(name = "joinDataProviderFactory")
    public JoinDataProviderFactory joinDataProviderFactory() {
        return new JoinDataProviderFactory(dataSourcesMap);
    }
}
