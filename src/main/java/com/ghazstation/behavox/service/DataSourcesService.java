package com.ghazstation.behavox.service;

import com.ghazstation.behavox.model.DataSourceInformation;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.model.ScanPageableTable;
import com.ghazstation.behavox.model.SeekOffsetPageableTable;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Map;

@AllArgsConstructor
public class DataSourcesService {
    @NonNull
    private final DataProviderFactoryService dataProviderFactoryService;

    @NonNull
    private final DataSourcesMap dataSourcesMap;

    public void add(@NonNull final DataSourceInformation dataSourceInformation) throws Exception {
        DataProvider dataProvider = create(dataSourceInformation);

        dataSourcesMap.add(dataSourceInformation.getDataSourceName(), dataProvider);
    }

    private DataProvider create(@NonNull final DataSourceInformation dataSourceInformation) throws Exception {
        return dataProviderFactoryService.getFactoryByType(dataSourceInformation.getDataType())
                .create(dataSourceInformation);
    }

    public ScanPageableTable scanTable(@NonNull final String dataProviderName,
                                       final int pageSize,
                                       final int pageNumber) {
        return get(dataProviderName).scanTable(pageSize, pageNumber);
    }

    public SeekOffsetPageableTable queryByColumn(@NonNull final String dataProviderName,
                                                 final String columnName,
                                                 final Object value,
                                                 final int seekLimit,
                                                 final int seekOffset) {
        return get(dataProviderName).queryByColumn(columnName, value, seekLimit, seekOffset);
    }

    public DataProvider get(@NonNull final String dataProviderName) {
        return dataSourcesMap.get(dataProviderName);
    }

    public Map<String, DataSourceInformation> getAllNamInformation() {
        return dataSourcesMap.getAllNamInformation();
    }
}
