package com.ghazstation.behavox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.time.LocalDateTime;

/**
 * A Wrapper for a Table with information about the Seek Offset this table corresponds to
 */
@Value
public class SeekOffsetPageableTable {
    private int foundRows;
    private int seekLimit;
    private int totalPages;
    private int seekPage;

    private Table dataTable;

    private LocalDateTime createTime;

    public boolean isLastOffset() {
        return seekPage == totalPages;
    }

    public boolean isFirstOffset() {
        return seekPage == 1;
    }

    public SeekOffsetPageableTable(int foundRows, int seekLimit, int totalPages, int seekPage, Table dataTable) {
        this(foundRows, seekLimit, totalPages, seekPage, dataTable, LocalDateTime.now());
    }

    @JsonCreator
    public SeekOffsetPageableTable(@JsonProperty("foundRows") int foundRows,
                                   @JsonProperty("seekLimit") int seekLimit,
                                   @JsonProperty("totalPages") int totalPages,
                                   @JsonProperty("seekPage") int seekPage,
                                   @JsonProperty("dataTable") Table dataTable,
                                   @JsonProperty("createTime") LocalDateTime createTime) {
        this.foundRows = foundRows;
        this.seekLimit = seekLimit;
        this.totalPages = totalPages;
        this.seekPage = seekPage;
        this.dataTable = dataTable;
        this.createTime = createTime;
    }
}
