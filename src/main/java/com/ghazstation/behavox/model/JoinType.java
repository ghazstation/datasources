package com.ghazstation.behavox.model;

public enum JoinType {
    INNER,
    LEFT_OUTER;
}