package com.ghazstation.behavox.join.strategy;

import com.ghazstation.behavox.model.JoinType;
import com.ghazstation.behavox.provider.DataProvider;
import com.ghazstation.behavox.model.ScanPageableTable;

import java.util.Arrays;
import java.util.List;

public interface JoinOperation {
    List<JoinOperation> SUPPORETED_JOINS =
            Arrays.asList(InnerJoin.SINGELTON, LeftOuterJoin.SINGELTON);

    JoinType getJoinType();

    ScanPageableTable performJoin(DataProvider dataProvider1,
                                  DataProvider dataProvider2,
                                  String column1,
                                  String column2,
                                  int pageSize,
                                  int pageNumber);
}
