package com.ghazstation.behavox.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SwaggerMvcTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void redirectBase() throws Exception {
        this.mvc.perform(get("/"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/swagger-ui.html"))
                .andExpect(content().string(""));
    }

    @Test
    public void pageNotFoundError() throws Exception {
        this.mvc.perform(get("/fdsfsdfs"))
                .andExpect(status().is4xxClientError());
    }
}
