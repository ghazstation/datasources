package com.ghazstation.behavox.csv;

import com.ghazstation.behavox.model.ColumnHeader;
import com.ghazstation.behavox.model.TableRow;
import com.ghazstation.behavox.model.Table;
import com.ghazstation.behavox.model.DataType;
import lombok.NonNull;

import java.util.*;

public class CsvDataFactory {

    public Table createTable(@NonNull final List<String[]> allRows) {
        int n = allRows.size();
        List<ColumnHeader> header = getHeader(allRows.get(0));
        List<TableRow> rows = new ArrayList<>();
        for (int i = 1; i < n; i++) {
            rows.add(getDataRow(allRows.get(i), header));
        }

        return new Table(header, rows);
    }

    private   List<ColumnHeader>  getHeader(@NonNull final String[] row) {
        List<ColumnHeader> headerList = new ArrayList<>();
        int n = row.length;
        for (int i =0 ; i < n;i++) {
            String[] split = row[i].trim().split(":");
            headerList.add(new ColumnHeader(split[0].trim(), DataType.getTypeFromName(split[1].trim()), i));
        }
        return headerList;
    }

    private TableRow getDataRow(@NonNull String[] row, @NonNull final List<ColumnHeader> headerList) {
        int n = row.length;
        List<Object> data = new ArrayList<>();
        for(int i = 0 ; i < n; i++) {
            data.add(headerList.get(i).getColumnType().convert(row[i].trim()));
        }
        return new TableRow(data);
    }
}
