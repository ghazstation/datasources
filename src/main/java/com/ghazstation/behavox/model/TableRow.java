package com.ghazstation.behavox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

/**
 * TableRow wraps data in a row, mapping to different kinds of objects and types
 */
@Value
public class TableRow {
    private List<Object> values;

    @JsonCreator
    public TableRow(@JsonProperty("values") List<Object> values) {
        this.values = values;
    }
}
