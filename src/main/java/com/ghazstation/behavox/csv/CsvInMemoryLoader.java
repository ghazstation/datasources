package com.ghazstation.behavox.csv;

import com.ghazstation.behavox.model.Table;
import com.opencsv.CSVReader;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

@AllArgsConstructor
public class CsvInMemoryLoader {
    @NonNull
    private final CsvDataFactory csvDataFactory;

    public Table load(@NonNull final String filePath) {
        try(CSVReader csvReader = new CSVReader(new FileReader(filePath))) {
            List<String[]> allRows = csvReader.readAll();

            return csvDataFactory.createTable(allRows);
        } catch (IOException ex) {
            return null;
        }
    }
}
