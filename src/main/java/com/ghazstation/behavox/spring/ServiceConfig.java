package com.ghazstation.behavox.spring;

import com.ghazstation.behavox.provider.DataProviderFactory;
import com.ghazstation.behavox.service.DataProviderFactoryService;
import com.ghazstation.behavox.service.DataSourcesMap;
import com.ghazstation.behavox.service.DataSourcesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ServiceConfig {

    @Autowired
    private List<DataProviderFactory> dataProviderFactoryList;

    @Bean
    public DataSourcesMap dataSourcesMap() {
        return new DataSourcesMap();
    }

    @Bean
    public DataProviderFactoryService dataProviderFactoryService() {
        return new DataProviderFactoryService(dataProviderFactoryList);
    }

    @Bean
    public DataSourcesService dataSourcesService() {
        return new DataSourcesService(dataProviderFactoryService(), dataSourcesMap());
    }
}
