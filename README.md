# Data Sources Joiner
This Spring Boot Application provides APIs to manage and join different sources of data
## API Documentation & Swagger UI
The API and its usage are best documented in Swagger and the Swagger UI
To check swagger UI, just point to service url in browser.
i.e. ''' localhost:8080 '''

## Running the app
The ''' run.sh ''' will build the package and run the web application
Application by default runs on localhost:8080

## Major Dependencies
* Spring Boot for building java web applications
* Swagger for Documenting and Interacting with APIs
* Maven for package management, Java 8 for building

## Testing
The application has a suite of unit tests and end to end tests that invoke the MVC app end to end and verify correct behavior.
Test coverage is high, but the tests could be improved with more complex testing scenarios for different interactions between different sources (a more end to end/integration tests)
* Full coverage report available at target\site\jacoco\index.html

# Important Limitations
* The application doesn't have any authorization or authentication security features
* The Caching System only works when default page or seeklimit of 100 is used
* Only Inner and Left Outer Joins supported for join operations
* Current CSV loader only reads from local file system. We should make it read from any URL as well as support reading from S3

# Data Sources Overview
## Overall design
This application serves as a prototype into how we would design a system that would allow us to 1. view 2. model and 3. query different sources of data.

The system is divided into several components
1. The Web APIs, which allow us to interface with the application
1. Data Sources Joiner
1. CSV Data Source Loader
1. Data Source Cache

Although in this prototype, all these components are included in the same APP, its advisable to split those up into seperate applications that can be seperately scaled and managed

The architecture would look something like this (with addition of other data sources)
```
                        [Web API Layer/Proxy Layer]
                            /               
                           /                 
                          v                   
[Join Engine]<-[Data Source Engine] --->   [Data source Cache]  ----> [Data source own database option]            
                /       |        \
               /        |         \
              V         v          v
[CSV Data Source]  [MySQL]   [Hadoop]  ..... [more]
```
Data Sources Web API supports
1. Adding new data source. This could be a CSV file, a MySQL, etc. 
2. Create a new 'view' that is a join of two other data sources and saving it as a view
3. Query any of the above data sources (Scan or Query columns)

Most of the data in the system can be generated on demand
* For puling any data from any data source, we map our Scan adn Query operations to appropriate database Scan adn Query operations
    * We can choose to cache any results from such a scan or query
* For pulling data from a Join Data Source, the data is generated on demand, using a Scan, Query and Get schematics of the underlying data sources
    * We store the results of such a join in a cache (and potentially an actual data store of our choosing)
    * Data for joins can be generated on demand as we scan/seek more values in the file
* Data can only be stored in cache once its calculated/queried so the system can be dynamic
    
## Building for real-time
Because we expect data to be generated in real-time, we employ the paging mechanism (and ability to generated joins on demand) to support real-time analytics
We can choose to only run queries to fill out our tables as data come in
Currently the system has no notion of time, but time can be added as a dimension to our IDs which will allow us to do real-time processing

* We would be able to do micro-batching in order to do real-time processing of the data

## Data Sources
Currently the application allows data sources for
* CSV files [com.ghazstation.behavox.csv.InMemoryCsvDataProvider]()
* Join of any two data sources [com.ghazstation.behavox.join.JoinCompositeDataProvider]()
* InMemoryCache of any data source [com.ghazstation.behavox.storage.CacheDataProvider]()

Any other types of data providers can be added by 
1. Implementing the [com.ghazstation.behavox.DataProvider]() Interface
1. Implementing the [com.ghazstation.behavox.DataProviderFactory]() Interface that can vend out DataProvider
1. (Optional) Extend a new  [com.ghazstation.behavox.model.DataSourceInformation]() for this new DataProvider
1. Creating a Spring Bean for DataProviderFactory and any required dependencies

##Query
We currently only support a scan operation and a query by column value
### Pagination
Scans and queries are paginated. 
For scans, the pagination is based on a page size and page number. You request pages from (page number - 1  * page size) to (page number * page size)
For queries, we employ an offset approach. We run the query against starting at seeklimit*seekPage-1 and we query seeklimit entries

## Caching
Caching is currently only working for pages of size a 100 and seeklimits of a 100
The reason is because its much simpler to reason about how much we are caching if that number is constant
IF we are to take this to production, I would suggest making a design decision to use something like 100 or a 1000 as hardcoded page size for the system

Also this cache is currently ephmeral, but it could be persistent storage (like HBase or MySQL or Object Store like S3)

## Schema
Data can have schema. The allowed DataTypes are modeled in [com.ghazstation.behavox.model.DataType]()
Currently String and Integers are supported. Others could be supported in future

## Data
Data is modeled in [com.ghazstation.behavox.model.Table](). A table contains columns which have schemas and rows

## How this is not optimal
The design we have is not optimal and we can improve a lot of things
* Data is not compressed. We can compress data that we read and store using something like Apache Avro. Java objects are not the optimal way to store things
* We wrote a lot of code, though projects ike Hive provide abstractions (along with other Big Data frameworks) that solve some of our problems
* Our queries are limited and don't support full SQL query (or another language). Hive could solve that also
* We can allow any page size but needing to add more smart logic to our service

##Scalability, Availability & Fault-Tolerance
If broken down into separate components (seperating engine from caching from CSV engine), would allow us to scale everythig horizontally
By spiting up caching/storage into its own components, queries across multiple hosts can coordinate


# Examples
## Create CSV File Data Source
curl -X POST "http://localhost:8080/v1/api/addDataSource" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"dataSourceName\": \"CSV_1\", \"dataType\": \"csv\", \"dataSourceUrl\":\"~/datasources/target/classes/CSV_1.csv\"}"
## On-demand Join Data Source Engine
curl -X POST "http://localhost:8080/v1/api/addDataSource" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"dataSourceName\": \"join\", \"dataType\": \"join\",\"dataSourceName1\" : \"CSV_1\",\"dataSourceName2\" : \"CSV_1_cache\",\"joinType\" : \"INNER\",\"dataSourceColumn1\":\"a\",\"dataSourceColumn2\":\"b\"}"
## Create CSV File Cache Data Source
curl -X POST "http://localhost:8080/v1/api/addDataSource" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"dataSourceName\": \"CSV_1_cache\", \"dataType\": \"csv-cache\", \"dataSourceUrl\":\"~/datasources/target/classes/CSV_1.csv\"}"
## Create Join Cache Data Source
curl -X POST "http://localhost:8080/v1/api/addDataSource" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"dataSourceName\": \"join-cached-table\", \"dataType\": \"join-cache\",\"dataSourceName1\" : \"CSV_1\",\"dataSourceName2\" : \"CSV_1_cache\",\"joinType\" : \"INNER\",\"dataSourceColumn1\":\"a\",\"dataSourceColumn2\":\"c\"}"

## Discover Available Data Sources
curl -X GET "http://localhost:8080/v1/api/datasources" -H "accept: */*"
## Get Information about specific data source
curl -vvv localhost:8080/v1/api/datasources/{datasourcename}

curl -X GET "http://localhost:8080/v1/api/datasources/CSV_1" -H "accept: */*"
## Scan
curl -X POST "http://localhost:8080/v1/api/scan?dataProviderName=CSV_1&pageNumber=1&pageSize=100" -H "accept: */*"
## Query
curl -X POST "http://localhost:8080/v1/api/searchByColumn?columnName=a&columnValue=2&dataProviderName=CSV_1&offsetLimit=100&offsetPage=1" -H "accept: */*"

# Meeting the design criteria
This prototype will work with 
* static or dynamically generated data since we can just query, and join on demand as number of pages grow. We don't need to know the number of data ponits. They are potentially infinite (We will have to do changes to move from int to BigInt in Java)
* provide a statically typed schema for columns that the data sources interface supports
* Supports search by column and scan operations 

