package com.ghazstation.behavox.helper;

import com.ghazstation.behavox.model.ColumnHeader;
import com.ghazstation.behavox.model.DataType;
import com.ghazstation.behavox.model.Table;
import com.ghazstation.behavox.model.TableRow;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TableHelper {
    public static String COL_NAME = "Name%d";
    public static String ROW_VALUE = "Value%d:%d";

    public static Table createTable(int columns, int rows) {

        List<ColumnHeader> headerRow = IntStream.range(0, columns)
                .boxed()
                .map(TableHelper::createColumn)
                .collect(Collectors.toList());

        List<TableRow> dataRows =  IntStream.range(0, rows)
                .boxed()
                .map(i -> createRow(i, columns))
                .collect(Collectors.toList());

        return new Table(headerRow, dataRows);
    }

    public static ColumnHeader createColumn(int i) {
        return new ColumnHeader(String.format(COL_NAME, i), DataType.STRING, i);
    }

    public static TableRow createRow(int i, int columns) {
       return new TableRow(IntStream.range(0, columns)
                .boxed()
               .map(col ->
                       String.format(ROW_VALUE, i, col))
                .collect(Collectors.toList()));
    }
}
