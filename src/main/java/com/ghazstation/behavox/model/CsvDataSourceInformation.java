package com.ghazstation.behavox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Input DataSourceInformation for creating CsvDataSource
 */
@ApiModel(parent = DataSourceInformation.class)
@Getter
@EqualsAndHashCode(callSuper = true)
public class CsvDataSourceInformation extends DataSourceInformation {
    @NotNull
    @Size(min = 3, max = 1024)
    @JsonProperty("dataSourceUrl")
    private String dataSourceUrl;

    @Builder
    @JsonCreator
    public CsvDataSourceInformation(@JsonProperty("dataType")String dataType,
                                    @JsonProperty("dataSourceName")String dataSourceName,
                                    @JsonProperty("dataSourceUrl")String dataSourceUrl) {
        super(dataType, dataSourceName);
        this.dataSourceUrl = dataSourceUrl;
    }
}
