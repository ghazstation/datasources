package com.ghazstation.behavox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Input DataSourceInformation for creating JoinDataSource
 */
@Getter
@EqualsAndHashCode(callSuper = true)
public class JoinDataSourceInformation extends DataSourceInformation {
    @NotNull
    @Size(min = 3, max = 50)
    private String dataSourceName1;

    @NotNull
    @Size(min = 3, max = 50)
    private String dataSourceName2;

    @NotNull
    private JoinType joinType;

    @NotNull
    @Size(min = 1)
    private String dataSourceColumn1;

    @NotNull
    @Size(min = 1)
    private String dataSourceColumn2;

    @Builder
    @JsonCreator
    public JoinDataSourceInformation(@JsonProperty("dataSourceName") String dataSourceName,
                                     @JsonProperty("dataSourceName1")String dataSourceName1,
                                     @JsonProperty("dataSourceName2")String dataSourceName2,
                                     @JsonProperty("joinType")JoinType joinType,
                                     @JsonProperty("dataSourceColumn1") String dataSourceColumn1,
                                     @JsonProperty("dataSourceColumn2") String dataSourceColumn2,
                                     @JsonProperty("dataType") String dataType) {
        super(dataType, dataSourceName);
        this.dataSourceName1 = dataSourceName1;
        this.dataSourceName2 = dataSourceName2;
        this.joinType = joinType;
        this.dataSourceColumn1 = dataSourceColumn1;
        this.dataSourceColumn2 = dataSourceColumn2;
    }
}
